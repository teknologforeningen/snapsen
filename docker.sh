#!/bin/bash
TAG=$(git describe --tags --always)
RUNOPTS=""
DIR="."
DFILE=""
CONTEXT="."
INPORT=8000
OUTPORT=8000

if [ $1 == "robot" ]
then
  TAG_PATH="snapsen/robot"
  DFILE="-f ./robot/Dockerfile"
  INPORT=8001
  OUTPORT=8001
  CONTEXT="./robot"
  # RUNOPTS=--shm-size="2g"

elif [ $1 == "nginx" ]
then
  TAG_PATH="snapsen/nginx"
  DFILE="-f ./nginx/Dockerfile"
  CONTEXT="./nginx"

elif [ $1 == "root" ]
then
  TAG_PATH="snapsen"
  DFILE="-f ./Dockerfile"

elif [ $1 == "test" ]
then
  TAG_PATH="snapsen/test"
  DFILE="-f ./test/Dockerfile"
  INPORT=8443
  OUTPORT="8443 -p 8000:8000"
fi

EXTRA_ARGS="${@:3}"
IMAGE_TAG="registry.gitlab.com/teknologforeningen/$TAG_PATH"

if [ $2 == "build" ]
then
  echo "Building image $1"
  echo "Dockerfile: $DFILE"
  echo "Context: $CONTEXT"
  echo "Extra args: $EXTRA_ARGS"
  BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
  BUILD_VERSION=$TAG
  VCS_REF=$(git rev-parse --verify HEAD)
  echo $BUILD_DATE
  echo $BUILD_VERSION
  echo $VCS_REF
  DOCKER_BUILDKIT=1 docker build \
    --tag $IMAGE_TAG:latest \
    --tag $IMAGE_TAG:$TAG \
    --progress=plain \
    --build-arg BUILD_DATE="$BUILD_DATE" \
    --build-arg BUILD_VERSION="$BUILD_VERSION" \
    --build-arg VCS_REF="$VCS_REF" \
    $DFILE \
    $CONTEXT \
    $EXTRA_ARGS

elif [ $2 == "run" ]
then
  docker run $RUNOPTS \
    --rm \
    -p $INPORT:$OUTPORT \
    -i $IMAGE_TAG:$TAG \
    $EXTRA_ARGS
elif [ $2 == "develop" ]
then
  docker run $RUNOPTS \
    --rm -i \
    -p $INPORT:$OUTPORT \
    -v $(pwd)/src:/snapsen/src \
    -v $(pwd)/gatsby:/snapsen/gatsby \
    -v $(pwd)/books:/snapsen/books \
    -v $(pwd)/plugins:/snapsen/plugins \
    -v $(pwd)/custom_assets:/snapsen/custom_assets \
    -v $(pwd)/static:/snapsen/static \
    -v $(pwd)/gatsby-browser.tsx:/snapsen/gatsby-browser.tsx \
    -v $(pwd)/gatsby-config.ts:/snapsen/gatsby-config.ts \
    -v $(pwd)/gatsby-node.ts:/snapsen/gatsby-node.ts \
    -v $(pwd)/gatsby-ssr.tsx:/snapsen/gatsby-ssr.tsx \
    -e NODE_OPTIONS=--no-experimental-fetch \
    -e NODE_ENV=development \
    -e GATSBY_ACTIVE_ENV=development \
    $IMAGE_TAG:$TAG \
    yarn develop -H 0.0.0.0 \
    $EXTRA_ARGS
fi
