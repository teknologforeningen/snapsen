// Generate theme-specific site icon
const im = require("imagemagick")

const iconPath = "./src/images/snapsen-icon-base.svg"
const tmpIconPath = "./.cache/snapsen-icon.svg"
const genIconPath = "./src/images/snapsen-icon.png"

try {
  console.log(`- Reading colors from ${styleYmlPath}`)
  const styleYml = yaml.load(fs.readFileSync(styleYmlPath, "utf8"))

  console.log(`- Reading SVG from ${iconPath}`)
  fs.readFile(iconPath, "utf8", (err, iconData) => {
    if (err) throw err
    const genIcon = iconData
      .replace("fill:#000000", `fill:${styleYml.colors.fg}`)
      .replace("fill:#ffffff", `fill:${styleYml.colors.bg}`)

    fs.writeFile(tmpIconPath, genIcon, function () {
      console.log(`- Site logo SVG generated: ${tmpIconPath}`)
      try {
        console.log(`- Creating PNG: ${genIconPath}`)
        im.convert([
          "-background",
          "none",
          tmpIconPath,
          "-resize",
          "500x500",
          "-background",
          "none",
          genIconPath,
        ])
      } catch (err) {
        throw err
      }
    })
  })
} catch (err) {
  console.log(err)
}
