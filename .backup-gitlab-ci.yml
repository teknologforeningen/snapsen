# Snapsen GitLab CI/CD pipeline

stages:
  - build
  - test
  - release

image: docker:20.10.17-git

include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  # Docker build
  DOCKER_BUILDKIT: 1
  CONTEXT: "."

  # CI git config
  GIT_STRATEGY: fetch
  GIT_DEPTH: 10
  GIT_FETCH_EXTRA_FLAGS: --prune --tags
  GIT_CLONE_PATH: $CI_BUILDS_DIR/snapsen

  # GitLab container registry URLs
  ROOT: $CI_REGISTRY_IMAGE
  NGINX: $CI_REGISTRY_IMAGE/nginx
  ROBOT: $CI_REGISTRY_IMAGE/robot
  TEST: $CI_REGISTRY_IMAGE/test

  # Dependency related
  SNAPSEN_BASE_IMG: node:18.13-alpine
  SNAPSEN_APK_DEPS: >-
    git
    ffmpeg
    build-base
    g++
    cairo-dev
    jpeg-dev
    pango-dev
    giflib-dev
    py3-pip

  # Caching
  CACHE_FALLBACK_KEY: default
  CACHE_APK: .buildkit-cache/apk
  CACHE_APT: robot/.buildkit-cache/apt
  CACHE_PIP: robot/.buildkit-cache/pip

  # Escape codes for color output
  TXT_CLR: "\e[0m"
  TXT_RED: "\e[31m"
  TXT_GRN: "\e[32m"
  TXT_YLW: "\e[33m"
  TXT_BLU: "\e[34m"
  TXT_MGT: "\e[35m"

# Package caches for building Docker containers
.apk_cache: &apk_cache
  key:
    files:
      - Dockerfile
      - yarn.lock
  paths:
    - $CACHE_APK
  policy: pull

cache: &apt_cache
  key:
    files:
      - robot/Dockerfile
  paths:
    - $CACHE_APT
  policy: pull

.pip_cache: &pip_cache
  key:
    files:
      - robot/Dockerfile
  paths:
    - $CACHE_PIP
  policy: pull

.public_cache: &public_cache
  paths:
    - public
  policy: pull-push

.docker-dind: &docker-dind
  - docker:20.10.17-dind

.docker-before: &docker-before
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - export BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
  - export BUILD_VERSION=$(git describe --tags --always)
  - export VCS_REF=$(git rev-parse --verify HEAD)

.docker-build: &docker-build
  - echo "Building $IMG_TAG with tag $CI_COMMIT_REF_SLUG"
  - >-
    docker build
    --cache-from $IMG_TAG:latest
    --cache-from $IMG_TAG:$CI_COMMIT_REF_SLUG
    $CACHE_FROM
    --tag $IMG_TAG:$CI_COMMIT_REF_SLUG
    --build-arg TAG_VERSION="$CI_COMMIT_REF_SLUG"
    --build-arg BUILD_DATE="$BUILD_DATE"
    --build-arg BUILD_VERSION="$BUILD_VERSION"
    --build-arg VCS_REF="$VCS_REF"
    --build-arg BUILDKIT_INLINE_CACHE=1
    -f $DOCKERFILE
    $CONTEXT

.docker-after-build: &docker-after-build
  - docker push $IMG_TAG:$CI_COMMIT_REF_SLUG

.docker-tag-and-push: &docker-tag-and-push
  - echo "Tagging $IMG_TAG:$CI_COMMIT_REF_SLUG with 'latest', $BUILD_VERSION, and pushing to registry"
  - docker pull $IMG_TAG:$CI_COMMIT_REF_SLUG
  - docker tag $IMG_TAG:$CI_COMMIT_REF_SLUG $IMG_TAG:latest
  - docker tag $IMG_TAG:$CI_COMMIT_REF_SLUG $IMG_TAG:$BUILD_VERSION
  - docker push $IMG_TAG:latest
  - docker push $IMG_TAG:$BUILD_VERSION

Populate apk and pip cache directories:
  stage: build
  allow_failure: true
  services: *docker-dind
  cache:
    - <<: *apk_cache
      policy: pull-push
    - <<: *pip_cache
      policy: pull-push
  before_script:
    - mkdir -p $CACHE_APK
    - mkdir -p $CACHE_PIP
  script:
    - >-
      docker run
      -v $GIT_CLONE_PATH/$CACHE_APK:/var/cache/apk
      -v $GIT_CLONE_PATH/$CACHE_PIP:/var/cache/pip
      -v $GIT_CLONE_PATH/yarn.lock:/yarn.lock
      -v $GIT_CLONE_PATH/package.json:/package.json
      -v $GIT_CLONE_PATH/robot/requirements.txt:/requirements.txt
      $SNAPSEN_BASE_IMG
      /bin/ash -c "ln -s /var/cache/apk /etc/apk/cache
      && apk add $SNAPSEN_APK_DEPS
      && PIP_CACHE_DIR=/var/cache/pip pip3 install -r requirements.txt"
    - echo -e "$TXT_BLU$(du -hs $CACHE_APK)$TXT_CLR"
    - echo -e "$TXT_BLU$(du -hs $CACHE_PIP)$TXT_CLR"

Populate apt cache directory:
  image: ubuntu:focal-20220531
  stage: build
  cache:
    - <<: *apt_cache
      policy: pull-push
  allow_failure: true
  before_script:
    - mkdir -p $CACHE_APT/partial
  script:
    - apt-get update
    - >-
      apt-get -d
      -o Dir::Cache::Archives="$GIT_CLONE_PATH/$CACHE_APT"
      -o Debug::Nolocking=1
      --yes
      install nginx python3-pip
    - echo -e "$TXT_BLU$(du -hs $CACHE_APT)$TXT_CLR"

Compile TypeScript:
  image: $ROOT:$CI_COMMIT_REF_SLUG
  stage: build
  allow_failure: true
  needs:
    - Build base container
  script:
    - yarn install --immutable
    - yarn tsc
  only:
    changes:
      - tsconfig.json
      - package.json
      - yarn.lock
      - gatsby-config.ts
      - ./*.{ts,tsx}
      - gatsby/*.{ts,tsx}
      - src/components/**/*.{ts,tsx}
      - src/**/*.{ts,tsx}
      - plugins/**/*.{ts,tsx}

Build base container:
  stage: build
  services: *docker-dind
  cache:
    - *apk_cache
  variables:
    IMG_TAG: $ROOT
    DOCKERFILE: Dockerfile
  script:
    - *docker-before
    - *docker-build
    - *docker-after-build

Build nginx container:
  stage: build
  services: *docker-dind
  variables:
    IMG_TAG: $NGINX
    DOCKERFILE: nginx/Dockerfile
    CONTEXT: nginx/
  script:
    - *docker-before
    - *docker-build
    - *docker-after-build

Build Snapsen test container:
  stage: build
  needs:
    - Build base container
    - Build nginx container
  services: *docker-dind
  variables:
    IMG_TAG: $TEST
    CACHE_FROM: >-
      --cache-from $ROOT:$CI_COMMIT_REF_SLUG
      --cache-from $NGINX:$CI_COMMIT_REF_SLUG
    DOCKERFILE: test/Dockerfile
  script:
    - *docker-before
      # Pull root and nginx image built in previous job
    - *docker-build
    - *docker-after-build

Run all Jest tests:
  image: $ROOT:$CI_COMMIT_REF_SLUG
  allow_failure: true
  needs:
    - Build base container
  stage: build
  cache:
    - *public_cache
      #  artifacts:
      #    when: always
      #    paths:
      #      - public
  script:
    - yarn install --immutable
    - yarn test:coverage
    - mkdir -p public/jest_$CI_PIPELINE_IID
    - cp -r coverage public/jest_$CI_PIPELINE_IID
    - ls -l public/jest_$CI_PIPELINE_IID
  after_script:
    - echo "<p><a href=\"jest_$CI_PIPELINE_IID/coverage/lcov-report/index.html\">$CI_COMMIT_BRANCH$CI_COMMIT_TAG -- Jest tests $CI_PIPELINE_IID</a></p>" >> public/index.html

Build RobotFramework container:
  stage: build
  cache:
    - *apt_cache
    - *pip_cache
  services: *docker-dind
  variables:
    IMG_TAG: $ROBOT
    DOCKERFILE: robot/Dockerfile
    CONTEXT: robot/
  script:
    - *docker-before
    - *docker-build
    - *docker-after-build

Run RobotFramework tests:
  stage: test
  image: ghcr.io/marketsquare/robotframework-browser/rfbrowser-stable:v13.3.0
  services:
    - name: $TEST:$CI_COMMIT_REF_SLUG
      alias: snapsen
    - name: docker:20.10.17-dind
  variables:
    URL: http://snapsen:8000/
    OUTPUTDIR: /home/pwuser/public/robot_browser_$CI_PIPELINE_IID
  cache:
    - *public_cache
      #  artifacts:
      #    when: always
      #    paths:
      #      - /home/pwuser/snapsen/public
  needs:
    - Build Snapsen test container
    - Run all Jest tests
  script:
    - mkdir -p $OUTPUTDIR
    - >-
      robot
      --variable URL:$URL
      --name "All RF tests"
      --outputdir $OUTPUTDIR
      --output alltests
      robot/*.robot
    - >-
      robot
      --variable URL:$URL
      --variable BOOKSLUG:local-test-book
      --variable SONGSLUG:gaudeamus-igitur
      --variable SONGTITLE:"Gaudeamus igitur"
      --variable SONGLYRICS:senectutem
      --name "Find Gaudeamus Igitur"
      --outputdir $OUTPUTDIR
      --output test2
      robot/find_song.robot
  after_script:
    - echo "<p><a href=\"robot_browser_$CI_PIPELINE_IID/index.html\">$CI_COMMIT_BRANCH$CI_COMMIT_TAG -- Robot tests $CI_PIPELINE_IID</a></p>" >> public/index.html
    - >-
      rebot
      --report $OUTPUTDIR/index.html
      --log $OUTPUTDIR/log.html
      --reporttitle "Robot tests for Snapsen $BUILD_VERSION"
      --doc "Commit $CI_COMMIT_SHORT_SHA made by $CI_COMMIT_AUTHOR at
      $CI_COMMIT_TIMESTAMP. Title of the commit is \"$CI_COMMIT_TITLE\". Job URL
      is: $CI_JOB_URL"
      $OUTPUTDIR/*.xml

Run RobotFramework console errors test:
  stage: test
  image: $ROBOT:$CI_COMMIT_REF_SLUG
  services:
    - name: $TEST:$CI_COMMIT_REF_SLUG
      alias: snapsentest
    - name: docker:20.10.17-dind
  variables:
    URL: http://snapsentest:8000/
    OUTPUTDIR: public/robot_$CI_PIPELINE_IID
  cache:
    - *public_cache
      #  artifacts:
      #    when: always
      #    paths:
      #      - public
  needs:
    - Build RobotFramework container
    - Build Snapsen test container
    - Run all Jest tests
  script:
    - mkdir -p $OUTPUTDIR
    - >-
      robot
      --variable URL:$URL
      --name "Console errors test"
      --outputdir $OUTPUTDIR
      --output console_errors
      robot/selenium/console_errors.robot
  after_script:
    - echo "<p><a href=\"robot_$CI_PIPELINE_IID/index.html\">$CI_COMMIT_BRANCH$CI_COMMIT_TAG -- Robot tests $CI_PIPELINE_IID</a></p>" >> public/index.html
    - >-
      rebot
      --report $OUTPUTDIR/index.html
      --log $OUTPUTDIR/log.html
      --reporttitle "Robot tests for Snapsen $BUILD_VERSION"
      --doc "Commit $CI_COMMIT_SHORT_SHA made by $CI_COMMIT_AUTHOR at
      $CI_COMMIT_TIMESTAMP. Title of the commit is \"$CI_COMMIT_TITLE\". Job URL
      is: $CI_JOB_URL"
      $OUTPUTDIR/*.xml

pages:
  stage: release
  allow_failure: true
  when: always
  cache:
    - *public_cache
  needs:
    - Run all Jest tests
    - Run RobotFramework tests
  script:
    - ls -l public
    - cat public/index.html
  artifacts:
    when: always
    paths:
      - public

Release new images:
  stage: release
  services: *docker-dind
  only:
    - tags
  script:
    - *docker-before
    - export IMG_TAG=$ROOT
    - *docker-tag-and-push
    - export IMG_TAG=$NGINX
    - *docker-tag-and-push
    - export IMG_TAG=$TEST
    - *docker-tag-and-push
    - export IMG_TAG=$ROBOT
    - *docker-tag-and-push

Create new release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating new release with version $CI_COMMIT_TAG"
  release:
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG"
