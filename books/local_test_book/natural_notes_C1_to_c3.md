---
title: Natural notes from C1 to c3
sheet: /media/natural_C1_to_c3.mxl
audio:
  - file: /media/natural_C1_to_c3.mp3
    name: MIDI with 36 vocalists
startnote:
  - note: "C1"
  - note: "D1"
  - note: "E1"
  - note: "F1"
  - note: "G1"
  - note: "A1"
  - note: "B1"
  - note: "C"
  - note: "D"
  - note: "E"
  - note: "F"
  - note: "G"
  - note: "A"
  - note: "B"
  - note: "c"
  - note: "d"
  - note: "e"
  - note: "f"
  - note: "g"
  - note: "a"
  - note: "b"
  - note: "c1"
  - note: "d1"
  - note: "e1"
  - note: "f1"
  - note: "g1"
  - note: "a1"
  - note: "b1"
  - note: "c2"
  - note: "d2"
  - note: "e2"
  - note: "f2"
  - note: "g2"
  - note: "a2"
  - note: "b2"
  - note: "c3"
---

Natural notes from C1 to c3
===========================

:note[__Aah!__]{text="with apoplectic fury:"}

> This is a good song for releasing stress. More importantly, it is also very
> good for testing the starting note player when developing Snapsen. If you
> intend to use this song for therapeutic purposes, follow these instructions:
>
>   - The positive benefits are increased if you perform this piece with
>     several vocalists.
>     - If this is the case, be sure to pick different starting notes for
>       maximal effect.
>   - Pick a tone you like (or several if you are really talented).
>   - Point at the sky with an accusatory finger.
>   - Let it all out with a strong _"Aah!"_
>   - Bask in the echoes of your performance.
