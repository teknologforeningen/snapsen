---
lyricsCredit: Lars T. Johansson och Ehrling Eliasson
sheet: ./media/alla_tallarna.mxl
audio:
  - file: ./media/alla_tallarna.mp3
    name: MIDI Piano
startnote:
  - note: g##4
  - note: e#4
  - note: c##4
  - note: a#3
  - note: f##3
  - note: d#3
  - note: b#2
  - note: g#2
  - note: e#2
  - note: c#2
  - note: a#1
  - note: f#1
  - note: d#1
  - note: b
  - note: g#
  - note: e
  - note: c#
  - note: A
  - note: F#
  - note: D
  - note: B1
  - note: G1
  - note: E1
  - note: C1
---
Alla tallarna
=============

Alla tallarna, alla tallarna,
alla stora, alla små.
Alla tallarna, alla tallarna
ska vi koka 'rännvin på.
Alla tallarna, alla tallarna,
ifrån roten till dess topp.
Alla tallarna, alla tallarna,
ska vi ta och 'ricka opp.
**Skål!**
