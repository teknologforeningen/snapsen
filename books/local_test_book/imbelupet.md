---
title: Imbelupet
type: ordered
melody: Kors på Idas grav
melodyCredit: Johan Göran Berger
songOrder: ["-1", "2", "3", "4", "5", "6", "7", "8"]
---
Imbelupet
=========

Imbelupet glaset står på bräcklig fot,
kalla pilsnerflaskor luta sig därmot.
Men därnere, miserere,
uti magens avgrundsdjup,
sitter djävulen och väntar på en sup.

... uti magens välvda valv,
vandrar djävulen och ropar på en Halv.

... uti magen härs och tvärs,
kilar djävulen och skriker på en Ters.

... uti magens djup så svart,
löper djävulen och skränar på en Qvart.

... uti magens labyrint,
irrar djävulen och tjoar på en Qvint.

... uti magens slingerväxt,
springer djävulen och skriar på en Sext.

... uti magen halvoppknäppt,
rusar själva Fan och vrålar på en Sept.

... uti magen an och av,
vankar djävulen och väser på Oktav.

![Djävulen uti magens välvda valv](media/uti-magens-välvda-valv.svg)

> I allmänhet sjungs den första versen eller den som motsvarar supens
> ordningstal.
