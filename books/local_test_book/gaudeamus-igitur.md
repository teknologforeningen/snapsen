---
title: Gaudeamus igitur
type: ceremonial
lyricsCredit: Bishop Strada 1267, C.W. Kindleben 1781
sheet: /media/gaudeamus-igitur.mxl
---
Gaudeamus igitur
================

:repeat[Gaudeamus igitur, Iuvenes dum sumus;]
Post iucundam iuventutem, Post molestam senectutem
:repeat[Nos habebit humus.]

:repeat[Ubi sunt, qui ante nos In mundo fuere?]
Vadite ad superos, Transite ad inferos,
:repeat[Ubi iam fuere.]

:repeat[Vita nostra brevis est, Brevi finietur;]
Venit mors velociter, Rapit nos atrociter;
:repeat[Nemini parcetur.]

:repeat[Vivat academia, Vivant professores,]
Vivat membrum quod libet, Vivant membra quae libet;
:repeat[Semper sint in flore.]

:repeat[Vivat et respublica Et qui illam regit,]
Vivat nostra civitas, Maecenatum caritas,
:repeat[Quae nos hic protegit.]

:repeat[Vivant omnes virgines, Faciles, formosae,]
Vivant et mulieres, Tenerae, amabiles,
:repeat[Bonae, laboriosae.]

:repeat[Pereat tristitia, Pereant osores,]
Pereat diabolus Quivis antiburschius,
:repeat[Atque irrisores.]
