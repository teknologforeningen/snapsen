---
title: Teknologvisan
melody: "March of the Södermanland Regiment"
lyricsCredit: "Erik Hedman"
page: [16]
sheet: /media/teknologvisan.mxl
musicalKey: C
type: ceremonial
startnote:
  - note: "c"
audio:
  - name: MIDI File
    file: /media/teknologvisan.mp3
---
# Teknologvisan

Här ska ni se ett *schack*
som aldrig tappar takt, _faderalla_.
Teknologer vi äro allihop,
med vin och kvinnor vi stå på en förtrolig fot.
Med tofs i mössan och sticka[^1] i vår hand,
jobba vi flitigt
dock helst uti ett vingårdsland.
På exkursioner och fester e’ vi med.
Teolog e’ man ej
och vi tacka vår lycka för de’.

:repeat[Var teknolog
han är en vingårdens man.
Varhelst i staden man såg
ja där rantar han,
faderampampampampampam[^2]
Lustgårdens frukt,
den plockar han
med vana händer.[^3]
Korta krikon, långa krikon,
tjocka krikon, trånga krikon
plockar han med lust
och ett i sänder.]

> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum accumsan
> lorem nisi, non commodo mi efficitur id. Ut volutpat dui vel dui ullamcorper
> commodo. Duis egestas eros quis dolor fringilla gravida. Sed gravida ut mi
> pretium fringilla. Praesent tincidunt nulla eu nibh elementum sollicitudin. Ut
> molestie, nisl consectetur ullamcorper consequat, lectus sem laoreet ligula, eu
> sagittis tellus felis vitae mauris. Morbi euismod justo at tellus vestibulum,
> at auctor nisi auctor. Nulla facilisi. Nunc eget dolor sed mi pellentesque
> maximus sed vitae enim. Aenean nec erat sed nulla ornare molestie non vel
> lorem. Praesent non dolor eget urna dictum ullamcorper. Aenean lobortis
> interdum est ut feugiat. Praesent suscipit, sem et ullamcorper auctor, est quam
> pretium eros, non auctor nibh dolor vel nibh. Morbi tincidunt nisl eu cursus
> pretium. Nullam in arcu velit.
>
> Morbi volutpat dolor nec pretium hendrerit. Integer luctus sit amet libero
> vitae tempor. Sed eget pellentesque mauris. Maecenas in dictum ex. Ut vulputate
> scelerisque nisl, ac bibendum sem convallis quis. Integer porttitor diam in
> dolor vestibulum, vitae dictum neque pellentesque. Nunc congue ligula fermentum
> metus mollis faucibus. Nunc consequat ultrices laoreet. Suspendisse accumsan
> hendrerit leo ut convallis. Ut maximus elementum gravida. Praesent porta eget
> tortor id tempus. Suspendisse lorem ex, sagittis porttitor venenatis eu,
> dapibus vel lorem. Maecenas vitae sagittis lorem. Ut sit amet quam in nisl
> tincidunt vulputate a ut quam. Vivamus ut molestie ante.
>
> Vivamus ac viverra ex. Donec eu euismod dui, eu faucibus purus. Suspendisse
> viverra lacus non nisl vulputate, fermentum ultrices risus accumsan. Ut sit
> amet iaculis magna. Morbi eu velit in velit sagittis pretium. Morbi quis
> convallis est. Nulla id mollis dui. Praesent volutpat varius massa, vitae
> tincidunt ligula accumsan nec. Donec bibendum luctus risus, et dapibus sapien
> volutpat eget. Sed auctor laoreet convallis. Sed sagittis aliquet placerat.
> Integer blandit ante ut posuere interdum. Vivamus in pulvinar nibh. Nam ac
> lacinia nunc. Sed dignissim, dui vitae dapibus efficitur, augue metus aliquam
> nisi, ac condimentum mi nisi eget purus. Aenean a justo a nunc dapibus pretium.

[^1]: snabbkalkylator med tunga och slid
[^2]: trummas på bordet
[^3]: på frackfest sjungs “med vita handskar på”
