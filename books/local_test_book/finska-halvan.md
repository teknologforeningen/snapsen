---
melodyCredit: Adolf Eduard Marschner, 1842
page:
  - 38
  - 39
musicalKey: f-moll, c
title: Finska halvan
type: ordered
melody: Gute Nacht
startnote:
  - note: "c1"
  - note: "c"
    clefSign: F
    clefLine: 2
songOrder:
  - "2"
audio:
  - name: MIDI recording
    voices:
      - file: /media/finska-halvan-t1.mp3
        name: Tenor 1
      - file: /media/finska-halvan-t2.mp3
        name: Tenor 2
      - file: /media/finska-halvan-b1.mp3
        name: Bass 1
      - file: /media/finska-halvan-b2.mp3
        name: Bass 2
  - name: Recording
    file: /media/Finska_Alla.mp3
sheet: /media/finska-halvan.mxl
---
Finska halvan
=============

::::paragraphify
Hur länge skall på borden
den lilla halvan stå?
Skall snart ej höras orden:
"Låt halvan gå, låt gå."
Det ärvda vikingsinne
till bägar'n trår igen,
:::repeat
:solo[Halvan går.]
:all[Halvan går.]
:solo[Låt halvan gå!]
:all[Halvan går.]
:::
Skål, gutår!
::::
