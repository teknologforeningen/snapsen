---
title: Bort allt vad oro gör
type: wine
melodyCredit: C.M. Bellman
lyricsCredit: C.M. Bellman, song number 17 from "Bacchi Tempel", 1783
---

Bort allt vad oro gör
=====================

Bort allt vad oro gör,
Bort allt vad hjärtat kväljer!
Bäst att man väljer
Bland dessa buteljer
Sin maglikör.

:repeat[Granne! gör du just som jag gör,
Vet denna oljan ger humör.
Vad det var läckert!
Vad var det? Rhenskt Bleckert!
Oui Ingenjeur[^1]!]

Bort allt vad oro gör,
Allt är ju stoft och aska,
Låt oss bli raska
Och tömma vår flaska
Bland bröderna.

:repeat[Granne! gör du just som jag gör,
Vet denna oljan ger humör
Vad det var mäktigt!
Vad var det? Jo präktigt,
Malaga -- ja.]

[^1]: Egentligen "Oui Monseigneur".
