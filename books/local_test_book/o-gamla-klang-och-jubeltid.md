---
title: O, gamla klang och jubeltid
lyricsCredit: August Lindh
melody: O alte Burschenherrlichkeit
page:
- 22
- 23
sheet: /media/o-gamla-klang-och-jubeltid.mxl
type: ceremonial
---

O, gamla klang och jubeltid
===========================

O, gamla klang och jubeltid,
ditt minne skall förbliva,
och än åt livets bistra strid
ett rosigt skimmer giva.
Snart tystnar allt vårt yra skämt,
vår sång blir stum, vårt glam förstämt;
:repeat[o, jerum, jerum, jerum,
o, quae mutatio rerum!]

Var äro de som kunde allt,
blott ej sin ära svika,
som voro män av äkta halt
och världens herrar lika?
De drogo bort från vin och sång
till vardagslivets tråk och tvång;
:repeat[o, jerum, jerum, jerum,
o, quae mutatio rerum!]

:note[Den ene vetenskap och vett
in i scholares mänger,]{text="filosofer:"}
:note[den andre i sitt anlets svett
på paragrafer vränger,]{text="jurister:"}
:note[en plåstrar själen som är skral,]{text="teologer:"}
:note[en lappar hop dess trasiga fodral;]{text="medicinare:"}
:repeat[o, jerum, jerum, jerum,
o, quae mutatio rerum!]

:note[En tämjer forsens vilda fall,]{text="väg- och vatten:"}
:note[En annan ger oss papper,]{text="fabrikare:"}
:note[En idkar maskinistens kall]{text="maskin:"}
:note[En mästrar volt så tapper]{text="elektrotekniker:"}
:note[En ritar hus, en mäter mark]{text="arkit. och lantmät:"}
:note[En blandar till mixtur så stark]{text="kemister:"}
:repeat[o, jerum, jerum, jerum,
o, quae mutatio rerum!]

Men hjärtat i en sann student
kan ingen tid förfrysa.
Den glädjeeld, som där han tänt,
hans hela liv skall lysa.
Det gamla skalet brustit har,
men kärnan finnes frisk dock kvar,
:repeat[och vad han än må mista,
den skall dock aldrig brista.]

Så sluten, bröder, fast vår krets
till glädjens värn och ära!
Trots allt vi tryggt och väl tillfreds
vår vänskap trohet svära.
Lyft bägarn högt och klinga, vän!
De gamla gudar leva än
:repeat[bland skålar och pokaler,
bland skålar och pokaler!]

> Texten är en översättning från tyskan (_Rückblick eines alten Burschen_
> författad av Eugen Höfling 1825). Den tredje versen är en direkt motsvarighet
> till den tyska förlagan och handlar om de fyra ursprungliga fakulteterna. Den
> fjärde versen är anpassad för teknologer.
