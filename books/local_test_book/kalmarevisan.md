---
title: Kalmarevisan
type: other
lyricsCredit: G. S. Kallstenius
---
Kalmarevisan
============

::::paragraphify
:::repeat
:solo[Uti Kalmare stad]
:all[ja, där finns det ingen kvast]
:::
förrän lördagen.
::::

:solo[Hej dick]
:all[hej dack]
:solo[jag slog i]
:all[och vi drack.]
:solo[Hej dickom dickom dack.]
:all[Hej dickom dickom dack.]
För uti Kalmare stad
ja, där finns det ingen kvast
förrän lördagen.

::::paragraphify
:::repeat
:solo[När som bonden kommer hem]
:all[kommer bondekvinnan ut.]
:::
Är så stor i sin trut.
::::

Hej dick...

::::paragraphify
:::repeat
:solo[Var är pengarna du fått?]
:all[Ja dom har jag supit upp]
:::
uppå Kalmare slott.
::::

Hej dick...

::::paragraphify
:::repeat
:solo[Jag skall mäla dig an]
:all[för vår kronbefallningsman]
:::
och du ska få skam.
::::

Hej dick...

::::paragraphify
:::repeat
:solo[Kronbefallningsmannen vår]
:all[satt på krogen igår]
:::
och var full som ett får.
::::

Hej dick...

::::paragraphify
:::repeat
:solo[Vad ska bonden ha till mat?]
:all[Sura sillar och potat.]
:::
Det blir sillsallat.
::::

Hej dick...

> Den första tryckta versionen av Kalmarevisan är från år 1776.
