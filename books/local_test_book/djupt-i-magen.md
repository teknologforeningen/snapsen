---
title: Djupt i magen
type: ordered
musicalKey: d-minor, d
melody: Näckens polska
melodyCredit: Arvid August Afzelius
lyricsCredit: trad., upptecknad av pappa George C., Budweiser 1998 (vers 2)
sheet: /media/djupt-i-magen.mxl
audio:
  - name: Recording
    voices:
      - name: Tenor
        file: /media/djupt-i-magen-hogre.mp3
      - name: Bass
        file: /media/djupt-i-magen-lagre.mp3
  - name: MIDI sound file
    voices:
      - name: Tenor
        file: /media/djupt-i-magen-midi-t1.mp3
      - name: Bass
        file: /media/djupt-i-magen-midi-t2.mp3
startnote:
  - note: "d"
---

Djupt i magen
=============

Djupt i magen under revbensspjällen,
helan vilar i djupan sal.
Genom krävan snabb liksom gasellen
halvan ilar så skön och sval.
Skälvande lik en till altar gången brud,
hela kroppen väntar på det ljuva ljud,
likt en suck -- genom skrovet går
:repeat[när lilla tersen till blindtarmen når!]

Sjukt i magen efter gårdagskvällen,
skallen lider de värsta kval,
rummet snurrar runt likt karusellen,
uti krävan finns berg och dal.
Räddningen i flaska fort till läppen förs,
hälls däri och genast kroppens lättnad hörs,
likt en suck -- genom skrovet går
:repeat[när ägglikören till blindtarmen når!]
