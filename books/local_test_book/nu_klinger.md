---
title: Nu klinger
lyricsCredit: Frode Rinnan
startnote:
  - note: "E"
  - note: "g#"
  - note: "cb"

---

Nu klinger
==========

Nu klinger igjennom den gamle stad
på ny en studentersang
og alle mann alle i rekke og rad
svinger op under begerklang
og mens borgerne våkner i køia
og hører den glade kang-kang
synger alle mann alle mann alle mann
alle mann alle mann alle mann:

Studenter i den gamle stad
ta vare på byens ry!
Husk på at jenter øl og dram
var kjempernes meny.
Og faller i alle mann alle
skal det gjalle fra alle mot sky:
La’ke byen få ro
men la den få merke
den er en studenterby!

:repeat[Og øl og dram og øl og dram]

I denne gamle staden satt
så mangen en konge stor
og hadde nok av øl fra fat
og piker ved sitt bord
og de laga bøljer i gata
når hjem ifra gilde de for
og nu sitter de alle mann alle i Valhall
og traller til oss i kor:

Studenter i den gamle...

Og øl og dram og øl og dram...

På Elgester var det liv
i klosteret dag og natt:
Der hadde de sin kagge
og der hadde de sin skatt.
De herjet i Nonnenes gate
og rullet og tullet og datt
og nu skuer de fra himmelen ned
og griper sin harpe fatt:

Studenter i den gamle...

:repeat[Og øl og dram og øl og dram]

Når vi er vandret hen
og staden hviler et øyeblikk
da kommer våre sønner
og tar op den gamle skikk.
En lek mellem muntre buteljer
samt aldri så litt’ erotikk.
Og så sitter vi i himmelen
og stemmer op vår replikk:

Studenter i den gamle...

> Cool norsk visa. Kan också sjungas av andra än norrmän.
