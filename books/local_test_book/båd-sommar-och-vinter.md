---
title: Båd' sommar och vinter
melody: Skolgossarnas vintervisa (Och nu är det vinter)
melodyCredit: L. Aug. Lundh
page:
- 126
type: seasonal
---
Båd’ sommar och vinter
======================

:::center
Båd’ sommar och vinter ner nubben den
slinter i pojkar och stinter, går bra, går
bra och nu vi ej töva vår nykterhet
pröva och sjunga och öva,
går bra, går bra! Ett visst
kvistfritt kvastskaft
och kaffekask-
dags
strax
sex
lax-
ar i
lax-
ask
går
bra,
går bra
den provet
kan klara, han kan utan fara ta
supen och svara; **går bra, går bra!**
:::
