---
title: Punschyrsel
type: punsch
melody: Gökvisa
---

Punschyrsel
===========

_herrar:_
:repeat[Punsch, punsch,
punsch, punsch,
punsch, punsch,
Alla sorters]
_Da capo al fine._

_damer:_
När supen runnit hädan
och maten lagts därpå.
När kaffet står på bordet
vad väntar vi då på?

:repeat[Jo punsch, jo punsch,
jo alla sorters punsch!]

Ja, den föll oss i smaken
nu ropar vi gutår.
Och koppen står där naken
och väntar på påtår

:repeat[Och punsch, och punsch,
och ännu mera punsch!]
