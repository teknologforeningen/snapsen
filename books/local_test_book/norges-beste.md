---
title: Norges beste
audio:
  - name: Recording (four voices)
    voices:
      - file: /media/norges-beste-t1.mp3
        name: Tenor 1
      - file: /media/norges-beste-t2.mp3
        name: Tenor 2
      - file: /media/norges-beste-b1.mp3
        name: Bas 1
      - file: /media/norges-beste-b2.mp3
        name: Bas 2
lyricsCredit: Travesty by Lundasångare
melody: Norges Fjeld
melodyCredit: Halfdan Kjerulf 1846l
musicalKey: h-moll, fiss
page:
  - 40
  - 41
sheet: /media/norges-beste.mxl
songOrder:
  - "2"
startnote:
  - note: "f#"
type: ordered
---
Norges beste
============

Norges beste, sild i papper,
den har inga fjäll!
Skönt den andra dagen,
fröjdar gommen, magen.
Har du flaskan oppe?
Töm dess sista droppe!
Halvan i dig häll!
I dig halvan häll!
