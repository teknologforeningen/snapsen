*** Settings ***
Documentation       Open song with audio files and check audio player functionality

Library             String
Library             Collections
Resource            snapsen_browser.resource


*** Variables ***
${SONGTITLE}            Norges beste
${SONGSLUG}             norges-beste
${BOOKSLUG}             local-test-book
${COLLAPSIBLETITLE}     Recording (four voices)


*** Test Cases ***
Open Norges Beste
  [Documentation]    Search for and open the song "Norges beste".
  Open Snapsen
  Search For And Open Song        ${SONGTITLE}    ${SONGTITLE}
  Ensure Song Is Open

Open Audio Player
    [Documentation]    Open the collapsible containing the audio player and verify that the controls are visible.
  Open Collapsible    ${COLLAPSIBLETITLE}
  Scroll To Element   ${PLAYERSEEKBAR}
  Get Element States  ${PLAYBUTTON}           contains    visible
  Get Element States  ${PLAYERTHUMB}          contains    visible
  Get Element States  ${PLAYERSEEKBAR}        contains    visible
  Get Text            ${PLAYERTIME}           ==          00:00

Play And Pause Audio
    [Documentation]    Press the play button twice, first to play and then to pause the audio.
  Scroll To Element   ${PLAYERSEEKBAR}
  Play Audio
  Pause Audio

Reset Seek
    [Documentation]    Move the seek bar handle all the way to the left.
  ${STARTPOS}=            Get BoundingBox     ${PLAYERTHUMB}
  ${WIDTH}=               Get Viewport Size   width
  Drag And Drop Relative To                   ${PLAYERTHUMB}  -${WIDTH}       5
  Get Text                ${PLAYERTIME}       ==              00:00
  ${POS}=                 Get BoundingBox     ${PLAYERTHUMB}
  Set Global Variable     ${G_STARTPOS}       ${POS}
  Should Be True          ${POS}[x] < ${STARTPOS}[x]

Seek To End
    [Documentation]    Move the seek bar handle all the way to the right.
  ${STARTPOS}=    Get BoundingBox     ${PLAYERTHUMB}
  ${WIDTH}=       Get Viewport Size   width
  ${DIST}=        Evaluate            ${WIDTH} - ${G_STARTPOS}[x] - ${G_STARTPOS}[x]
  Drag And Drop Relative To           ${PLAYERTHUMB}      ${DIST}     5
  ${MAXTIME}=     Get Text            ${PLAYERMAXTIME}
  ${POS}=         Get BoundingBox     ${PLAYERTHUMB}
  Get Text        ${PLAYERTIME}       ==                  ${MAXTIME}
  Should Be True  ${POS}[x] > ${STARTPOS}[x]
  Ensure Console Does Not Contain Errors



*** Keywords ***
Play Audio
    [Documentation]    Click the play button. Ensure seek bar handle starts moving
    ...    and the current time changes
  ${startpos}=        Get BoundingBox     ${PLAYERTHUMB}
  ${starttime}=       Get Text            ${PLAYERTIME}
  Scroll To Element   ${PLAYBUTTON}
  Get Element States  ${PLAYBUTTON}       contains        visible
  Click With Options               ${PLAYBUTTON}         delay=10ms
  Sleep               2
  Get Text            ${PLAYERTIME}       !=              ${starttime}

Pause Audio
    [Documentation]    Click the pause button. Ensure seek bar handle stops moving
    ...    and the current time stops increasing.
  ${POS}=             Get BoundingBox     ${PLAYERTHUMB}
  ${CURRENTTIME}=     Get Text            ${PLAYERTIME}
  ${CURRENTSEC}=      Remove String Using Regexp          ${CURRENTTIME}          ^\\d\\d:\\d
  Scroll To Element   ${PAUSEBUTTON}
  Get Element States  ${PAUSEBUTTON}      contains        visible
  Click With Options               ${PAUSEBUTTON}     delay=10ms
  Sleep               1
  ${PAUSESEC}=        Remove String Using Regexp          ${CURRENTTIME}          ^\\d\\d:\\d
  Should Be True      ${PAUSESEC} - ${CURRENTSEC} <= 2
