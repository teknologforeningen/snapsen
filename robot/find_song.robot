*** Settings ***
Documentation       Search and find a single song

Resource            snapsen_browser.resource


*** Test Cases ***
Start Test
  Open Snapsen

Refresh Page And Check Console
  [Documentation]    Refresh main page and ensure no console errors are present.
  Sleep   1
  Reload
  Sleep    1
  Ensure Console Does Not Contain Errors

Search For Song Title And Reset
    [Documentation]    Search for ${SONGTITLE}. After result has been found,
    ...    clear and unfocus the search bar. Afterwards checks
    ...    that browser has returned to the main page.

  Search For Song     ${SONGTITLE}    ${SONGTITLE}
  Clear Search Bar
  Unfocus Search Bar
  Get Url             ==              ${URL}
  Ensure Console Does Not Contain Errors

Search For Song Lyrics And Reset
    [Documentation]    Search for ${SONGLYRICS}. After result has been found,
    ...    clear and unfocus the search bar. Afterwards checks
    ...    that browser has returned to the main page.

  Search For Song     ${SONGLYRICS}   ${SONGTITLE}
  Clear Search Bar
  Unfocus Search Bar
  Get Url             ==              ${URL}
  Ensure Console Does Not Contain Errors

Search For Song Title And Open Details
    [Documentation]    Search for ${SONGLYRICS}. After result has been found,
    ...    click the song card and ensure that the song details
    ...    page is opened.

  Search For And Open Song                        ${SONGTITLE}            ${SONGTITLE}
  Ensure Song Is Open
  Ensure Console Does Not Contain Errors

Close Song Details
    [Documentation]    Return to the main page by clicking the back button
    ...    (in the browser). Unfocus the search bar and ensure
    ...    the main page is open.
  Click   id=back-button
  Ensure Page Contains Songs
  Ensure Console Does Not Contain Errors

Ensure Console Error Is Caught
    New Page    ${URL}/this-page-does-not-exist
    Run Keyword And Expect Error    *   Ensure Console Does Not Contain Errors
