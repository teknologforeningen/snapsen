#!/usr/bin/env bash

# Start standard entry point script from Selenium container
/opt/bin/entry_point.sh &

# Sleep to let Selenium start properly
sleep 5

# Run commands given at runtime as non-privileged user
# su $(id -un 1200)

"$@"
