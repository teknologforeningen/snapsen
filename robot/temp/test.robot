*** Settings ***
Documentation    Open Snapsen and check for console errors.
Library          Collections
Library          Browser          jsextension=${CURDIR}/capturelogs.js
Resource         snapsen_browser.resource


*** Variables ***
${SONGTITLE}          Norges beste
${SONGSLUG}           norges-beste
${BOOKSLUG}           local-test-book
${COLLAPSIBLETITLE}   Recording (four voices)
${LVL_SEVERE}         SEVERE
${LVL_ERROR}          ERROR

*** Test Cases ***
Read Console
  [Documentation]    Open main page and ensure no console errors are present.
  Open Snapsen
  CaptureLogs
  Reload
  CaptureLogs
  Sleep           1
  # Ensure Console Does Not Contain Errors

Refresh Page And Read Console
  [Documentation]    Refresh main page and ensure no console errors are present.
  Sleep   1

#Search For Song And Check For Console Errors
#  [Documentation]     Search for song and ensure no console errors are present.
#  Search For Song     ${SONGTITLE}    ${SONGTITLE}
#  Clear Search Bar
#  Unfocus Search Bar
#  Sleep                         1
#  Ensure Console Does Not Contain Errors
#
#Search For And Open Song And Check For Console Errors
#  [Documentation]               Search for and open song and ensure no console errors are present
#  Search For And Open Song      ${SONGTITLE}      ${SONGTITLE}    ${BOOKSLUG}   ${SONGSLUG}
#  Sleep                         1
#  Ensure Console Does Not Contain Errors


*** Keywords ***
Ensure Console Does Not Contain Errors
  [Documentation]     Loop through the logged console messages and
  ...                 ensure no errors are present.
  ${consolelog}=  []
  Log Many        ${consolelog}
  FOR                               ${msg}               IN      @{consolelog}
    ${msg_level}=   Get From Dictionary     ${msg}  level
    Should Not Be Equal As Strings          ${LVL_SEVERE}   ${msg_level}
    Should Not Be Equal As Strings          ${LVL_ERROR}    ${msg_level}
  END
