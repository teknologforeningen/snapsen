async function CaptureLogs(page, logger) {
  logger("Console messages:")
  console.error("TESTERROR")
  page.on("console", (message) => {
    logger(message.text())
  })
}
CaptureLogs.rfdoc = "Return console messages."
exports.__esModule = true
exports.captureLogs = CaptureLogs
