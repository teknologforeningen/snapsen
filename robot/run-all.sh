#!/usr/bin/env bash

# Set vars

# Random hex string added to container and network names to avoid collisions
RAND=$(openssl rand -hex 2)
URL=http://snapsen-$RAND:8000/
TEST_IMAGE_TAG=$(git describe --tags --always)
TEST_IMAGE="registry.gitlab.com/teknologforeningen/snapsen/test:$TEST_IMAGE_TAG"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Kill the Snapsen container when script receives SIGHUP/SIGINT/SIGTERM
function cleanup {
  kill $SNAPSENPID
  sleep 5
  docker network rm snapsen-rfbrowser-test-$RAND
  exit
}
trap cleanup SIGHUP SIGINT SIGTERM

# Ensure pwd is the root repo dir
cd $SCRIPT_DIR/..

# Run all *.robot tests
TESTS=$(ls ./robot/*.robot)

# Build current version of Snapsen, and run it in docker
#./docker.sh root build
#./docker.sh test build
#./docker.sh nginx build
docker network create snapsen-rfbrowser-test-$RAND
docker run \
  --network "snapsen-rfbrowser-test-"$RAND \
  --name "snapsen-"$RAND \
  $TEST_IMAGE > /dev/null 2>&1 &

SNAPSENPID=$!


# Run RF tests in docker, and kill the Snapsen container afterwards
docker run  \
  --rm \
  --network snapsen-rfbrowser-test-$RAND \
  --mount type=bind,source="$(pwd)"/robot,target=/robot \
  ghcr.io/marketsquare/robotframework-browser/rfbrowser-stable:18 \
  robot \
    --variable URL:$URL \
    --name "All RF tests" \
    --outputdir /tmp \
    --output alltests \
    $TESTS \
    && cleanup

wait $SNAPSENPID
