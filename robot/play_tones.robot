*** Settings ***
Documentation       Search and find a single song

Resource            snapsen_browser.resource

*** Variables ***
${TONEBUTTON}     xpath=//button[contains(@id, 'notePlayer')]
${SONGTITLE}      Natural notes from C1 to c3
${SONGSLUG}       natural-notes-from-c1-to-c3
${SONGLYRICS}     Aah!
${COLLAPSIBLETITLE}    starting tones

*** Test Cases ***
Start Test
  Open Snapsen

Search For Song Title And Open Details
    [Documentation]    Search for ${SONGLYRICS}. After result has been found,
    ...    click the song card and ensure that the song details
    ...    page is opened.

  Search For And Open Song                        ${SONGTITLE}            ${SONGTITLE}

Press Tone Buttons
  [Documentation]     Open collapsible containing start tone buttons, get all tone button elements, and click them.
  Open Collapsible        ${COLLAPSIBLETITLE}
  # NOTE: The loop below should work, but currently throws an error
  # ${all tone buttons} =    Get Elements     ${TONEBUTTON}
  # FOR     ${button}     IN    ${all tone buttons}
  #     Click     ${button}         delay=500ms
  # END
  ${tonebtn count} =      Get Element Count       ${TONEBUTTON}
  FOR   ${index}    IN RANGE   ${tonebtn count}
    ${tonebtns} =           Get Elements            ${TONEBUTTON}
    Click With Options             ${tonebtns}[${index}]   delay=50ms
    Ensure Song Is Open
  END


Close Song Details
    [Documentation]    Return to the main page by clicking the back button
    ...    (in the browser). Unfocus the search bar and ensure
    ...    the main page is open.
  Ensure Console Does Not Contain Errors
  Click   id=back-button
  Ensure Page Contains Songs
