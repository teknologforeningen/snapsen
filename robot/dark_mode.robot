*** Settings ***
Documentation       Ensure dark mode switch is working as intended.

Library             Collections
Resource            snapsen_browser.resource

*** Variables ***
@{CSSCOLORS}       --fg    --bg    --primary    --primary75
                   ...  --primary50    --primary25    --neutral0
                   ...  --neutral5    --neutral10    --neutral20
                   ...  --neutral30    --neutral40    --neutral50
                   ...  --neutral60    --neutral70    --neutral80
                   ...  --neutral90
${LIGHTCOLOR}      --light
${DARKCOLOR}       --dark
${TOGGLESEL}       id=dark-mode-toggle
@{CSSPROPS}        color
@{TOGGLEPROPS}     border   background

*** Keywords ***
Get Colors From Wrapper
  [Documentation]    Get values of CSS color variables from the wrapper element.
  ${light colors}=    Create Dictionary
  FOR    ${color}    IN    @{CSSCOLORS}
    ${wrapper style} =  Get Style           id=wrapper  ${color}
    Set To Dictionary   ${light colors}     ${color}    ${wrapper style}
  END

  RETURN            ${light colors}

Toggle Switch
    [Documentation]     Click the dark mode button, and ensure the class of the body element switches.
    ${body classes}=    Get Classes        body
    Click       ${TOGGLESEL}
    IF    "light" in ${body classes}
      ${new body classes}=    Get Classes    body      validate     "dark" in value
    ELSE IF    "dark" in ${body classes}
      ${new body classes}=    Get Classes    body      validate     "light" in value
    ELSE
      Fail       The body element has neither the "dark" not the "light" class.
      ...        Classes before toggle: ${body classes}
      ...        Classes after toggle: ${new body classes}
    END

Toggle Switch And Ensure Element Colors Change
  [Arguments]     ${selector}             @{properties}
  FOR   ${prop}   IN   @{properties}
    ${light}=   Get Style               ${selector}     ${prop}
    Toggle Switch
    ${dark}=    Get Style               ${selector}     ${prop}
    Should Not Be Equal As Strings      ${light}        ${dark}      msg=The CSS property ${prop} of element with selector ${selector} is unchanged after toggling dark mode
  END


*** Test Cases ***
Start Test
  Open Snapsen

Toggle Switch And Ensure Wrapper Colors Change
  ${light colors}=    Get Colors From Wrapper
  Toggle Switch
  ${dark colors}=     Get Colors From Wrapper

  FOR    ${color}    IN    @{CSSCOLORS}
    Should Not Be Equal As Strings                  ${light colors}[${color}]                       ${dark colors}[${color}]
  END

Ensure Main Page Element Colors Switch
  Toggle Switch And Ensure Element Colors Change  ${SONGCARD}             @{CSSPROPS}
  Toggle Switch And Ensure Element Colors Change  ${TOGGLESEL}             @{TOGGLEPROPS}

Ensure Song Element Colors Switch
  Search For And Open Song     ${SONGTITLE}    ${SONGTITLE}
  Toggle Switch And Ensure Element Colors Change     p        color
  Toggle Switch And Ensure Element Colors Change     h1        color
