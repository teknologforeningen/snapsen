import * as React from "react"
import { graphql } from "gatsby"
import { Song, ToneAudiosprite } from "../types/song"
import SongDetails from "../components/Song/SongDetails"
import NotFoundPage from "../pages/404"
import SEO from "../components/Layout/SEO"

interface SongTemplateProps {
  pageContext: {
    songSlug: string
    bookSlug: string
  }

  data: {
    songQueryResult: Song
    siteQuery: any
    toneAudiospriteQuery: ToneAudiosprite
  }
}

/**
 * Template used by each song details page
 */
const SongTemplate: React.FunctionComponent<SongTemplateProps> = ({
  data: {
    songQueryResult: song,
    siteQuery: site,
    toneAudiospriteQuery: toneAs,
  },
}) => {
  const songUrl = new URL(
    `${song.book.slug}-${song.slug}`,
    site.siteMetadata.siteUrl
  ).toString()

  const startnotes = song?.startnote
  return song ? (
    <SongDetails
      title={song.title}
      lyrics={song.html}
      largeSheet={song.largeSheet && song.largeSheet.publicURL}
      mediumSheet={song.mediumSheet && song.mediumSheet.publicURL}
      smallSheet={song.smallSheet && song.smallSheet.publicURL}
      tag={song.tag}
      audio={song.audio}
      sheetOnly={site.siteMetadata.sheetOnly}
      url={songUrl}
      songStartnotes={startnotes}
      toneAuSpMeta={JSON.parse(toneAs?.internal?.content)}
      toneAuSp={toneAs?.childrenFile}
      {...song.frontmatter}
    />
  ) : (
    <NotFoundPage />
  )
}

export const songPageQuery = graphql`
  query ($songSlug: String!, $bookSlug: String!, $language: String!) {
    songQueryResult: markdownRemark(
      slug: { eq: $songSlug }
      book: { slug: { eq: $bookSlug } }
    ) {
      slug
      title
      excerpt
      book {
        title
        slug
      }
      largeSheet {
        publicURL
      }
      mediumSheet {
        publicURL
      }
      smallSheet {
        publicURL
      }
      tag {
        tag
        value
        label
        color
      }
      html
      audio {
        name
        file {
          publicURL
        }
        voices {
          name
          startTime
          duration
        }
      }
      startnote {
        name
        svgFile {
          publicURL
        }
        noteStep
        noteOctave
        clefLine
        clefOctaveChange
        clefSign
        noteAlter
      }
      frontmatter {
        page
        startnote {
          note
          clefSign
          clefLine
          clefOctaveChange
        }
        lyricsCredit
        melody
        melodyCredit
        type
        musicalKey
        songOrder
        pre
      }
    }

    siteQuery: site {
      siteMetadata {
        sheetOnly
        title
        siteUrl
      }
    }
    toneAudiospriteQuery: file(
      name: { eq: "snapsenToneAudiospriteMeta" }
      parent: { id: { eq: "Site" } }
    ) {
      name
      internal {
        content
      }
      childrenFile {
        extension
        publicURL
      }
    }
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`

export const Head = ({ data: { songQueryResult: song, siteQuery: site } }) => {
  const songPageDesc = `Läs sången "${song.title}" på ${site.siteMetadata.title}.`
  const songPageTitle = song.title + " – " + site.siteMetadata.title
  return (
    <SEO
      title={songPageTitle}
      description={songPageDesc}
      pathname={`${song.book.slug}-${song.slug}`}
    >
      <meta name={"og:type"} content={"article"} id={"og_type"} />
    </SEO>
  )
}

export default SongTemplate
