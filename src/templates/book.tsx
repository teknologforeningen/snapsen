import * as React from "react"
import { Book } from "../types/book"
import SearchResults from "../components/Search/SearchResults"

interface BookTemplateProps {
  data: {
    bookQueryResult: Book
  }
}

/**
 * Template used by each book details page
 */
const BookTemplate: React.FunctionComponent<BookTemplateProps> = ({
  data: { bookQueryResult: book },
}) => {
  return <SearchResults></SearchResults>
}

export default BookTemplate
