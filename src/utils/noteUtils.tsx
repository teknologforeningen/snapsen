import React from "react"
import { Note, FrontmatterNote } from "../types/song"

const NOTES = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

/** Get distance (in semitones) between two Notes.
 *
 * @param note1 - Reference note to measure distance from.
 * @param note2 - Note to measure distance to.
 * @returns Distance between notes in semitones. Negative number means that note2 is lower in pitch than note1 and vice versa.
 *
 */
export const getNoteDistance = (note1: Note, note2: Note) => {
  const stepDiff =
    NOTES.indexOf(note2.noteStep) +
    note2.noteAlter -
    (NOTES.indexOf(note1.noteStep) + note1.noteAlter)
  const octaveDiff = note2.noteOctave - note1.noteOctave

  return stepDiff + 12 * octaveDiff
}

export const getNoteNodeName = ({
  noteStep,
  noteAlter,
  noteOctave,
  clefSign,
  clefLine,
  clefOctaveChange,
}: Note): string => {
  const nameTail =
    typeof clefSign === "string" &&
    typeof clefLine === "number" &&
    !Number.isNaN(clefLine) &&
    typeof clefOctaveChange === "number" &&
    !Number.isNaN(clefOctaveChange)
      ? [clefSign, clefLine, clefOctaveChange]
      : []
  const nameSegments = [
    "singleNoteMxl",
    noteStep,
    noteAlter,
    noteOctave,
    ...nameTail,
  ]
  return nameSegments.join("_")
}

export const getNoteNodeParams = (noteNodeName: string): Note => {
  const p = noteNodeName.split("_")
  return {
    noteStep: p[1],
    noteAlter: parseInt(p[2]),
    noteOctave: parseInt(p[3]),
    clefSign: p[4],
    clefLine: parseInt(p[5]),
    clefOctaveChange: parseInt(p[6]),
  }
}

export const getNoteLabel = (note: Note, notation?: string) => {
  var noteLabel: string
  var noteOctaveMod: string
  if (!note?.noteOctave || !note?.noteStep) {
    return (
      <>
        {"N/A"}
        {<sub>{"?"}</sub>}
      </>
    )
  }
  switch (notation) {
    case "scientific":
      noteLabel = note.noteStep.toUpperCase()
      noteOctaveMod = note.noteOctave.toString()
      break

    case "helmholtz":
    default:
      noteLabel =
        note.noteOctave >= 3
          ? note.noteStep.toLowerCase()
          : note.noteStep.toUpperCase()

      noteOctaveMod =
        note.noteOctave === 3 || note.noteOctave === 2
          ? ""
          : note.noteOctave > 3
            ? (note.noteOctave - 3).toString()
            : //When note.noteOctave < 2
              (2 - note.noteOctave).toString()
      break
  }

  const noteSign =
    note.noteAlter === -1
      ? "♭"
      : note.noteAlter === -2
        ? "𝄫"
        : note.noteAlter === 1
          ? "♯"
          : note.noteAlter === 2
            ? "𝄪"
            : ""

  return (
    <>
      {`${noteLabel}${noteSign}`}
      {noteOctaveMod !== "0" && <sub>{noteOctaveMod}</sub>}
    </>
  )
}

/** Gets noteAlter from note shorthand string.
 *
 * Takes a string containing shorthand description of a single note, and
 * returns the noteAlter MXL attribute, i.e. the change in pitch of the root
 * noteStep.
 *
 * @param {string}    noteShorthand   Note shorthand.
 *
 * @return {number}   The noteAlter of the note described by noteShorthand.
 */
export const noteShorthand2NoteAlter = (noteShorthand: string): number => {
  // In case the first character is "b", strip it.
  const sh =
    noteShorthand.slice(0, 1) === "b"
      ? noteShorthand.slice(1, -1)
      : noteShorthand

  // Regex that matches groups of characters containing the characters '#' and
  // 'b'. Only the first match is used to determine noteAlter.
  const noteAlterRe = /[#b]+/
  const match = sh.match(noteAlterRe)
  const noteAlterStr: string = match && match.length > 0 ? match[0] : ""
  var noteAlter = 0

  // Go through the first match character by character. In case the char
  // matches '#', raise noteAlter by one. In case the char matches 'b', lower
  // noteAlter by one.
  if (noteAlterStr && noteAlterStr.length > 0) {
    for (var i = 0; i < noteAlterStr.length; i++) {
      if (noteAlterStr.charAt(i) === "#") {
        noteAlter += 1
      } else if (noteAlterStr.charAt(i) === "b") {
        noteAlter -= 1
      }
    }
  }

  return noteAlter
}

/** Gets noteOctave from note shorthand string.
 *
 * @param {string}    noteShorthand   Note shorthand.
 * @param {string}    notation        Which notation noteShorthand uses. Either 'helholtz' (which is the default), or 'scientific'.
 *
 * @return {number}   The noteOctave of the note described by noteShorthand.
 */
export const noteShorthand2NoteOctave = (
  noteShorthand: string,
  notation?: string
): number => {
  // Regex that matches groups of characters containing numbers 0-9. Only the
  // first matched group is used to determine noteOctave.
  const noteOctaveModRe = /[0-9]+/g
  var noteOctaveModNum: number
  var noteOctave = 0
  const pi = parseInt(
    noteShorthand.match(noteOctaveModRe)?.slice(0, 1) as unknown as string
  )
  if (typeof pi === "number" && !isNaN(pi)) {
    noteOctaveModNum = pi
  } else {
    noteOctaveModNum = 0
  }

  switch (notation) {
    case "scientific":
      // In case scientific pitch notation is used, set the note octave
      // directly to the first number found in the noteShorthand string.
      noteOctave = noteOctaveModNum
      break

    case "helmholtz":
    default:
      // If the first character (the note value) is lower case, set octave to
      // 3. If noteOctaveModNum is a number, lower the octave according to that
      // number.
      //
      // If the first character (the note value) is upper case, set octave to
      // 2. If noteOctaveModNum is a number, raise the octave according to that
      // number.

      // Get note step from first character in string
      const noteStep = noteShorthand.slice(0, 1)
      const isNoteOctaveLowerCase = noteStep === noteStep.toLowerCase()

      if (isNoteOctaveLowerCase) {
        noteOctave = 3
        if (typeof noteOctaveModNum === "number" && !isNaN(noteOctaveModNum)) {
          noteOctave = noteOctave + noteOctaveModNum
        }
      } else {
        noteOctave = 2
        if (typeof noteOctaveModNum === "number" && !isNaN(noteOctaveModNum)) {
          noteOctave = noteOctave - noteOctaveModNum
        }
      }
  }

  return noteOctave
}

export const noteShorthand2Note = (
  note: string,
  clefSign?: string,
  clefLine?: number,
  clefOctaveChange?: number,
  notation?: string
): Note => {
  // Remove whitespace from string
  const noteShorthandStrip = note.replace(/\s/, "")

  // Get note step from first character in string
  const noteStep = noteShorthandStrip.slice(0, 1)

  // Get note alter from '#' and 'b' characters in string.
  const noteAlter = noteShorthand2NoteAlter(noteShorthandStrip)

  // Get note octave.
  const noteOctave = noteShorthand2NoteOctave(noteShorthandStrip, notation)

  // If no clef parameters are given, attempt to select suitable clef based on
  // note pitch.

  var coc: number
  if (
    typeof clefOctaveChange === "undefined" ||
    Number.isNaN(clefOctaveChange)
  ) {
    coc = 0
  } else {
    coc = clefOctaveChange
  }

  if (typeof clefSign === "undefined" || Number.isNaN(clefOctaveChange)) {
    if (noteOctave >= 5) {
      clefSign = "G"
    } else if (noteOctave < 5 && noteOctave >= 3) {
      clefSign = "G"
      if (typeof clefOctaveChange === "undefined") {
        coc = -1
      }
    } else {
      clefSign = "F"
    }
  }

  if (typeof clefLine === "undefined" || Number.isNaN(clefLine)) {
    switch (clefSign) {
      case "G":
        clefLine = 2
        break
      case "F":
        clefLine = 4
        break
      default:
        // Assume middle C
        clefLine = 3
    }
  }

  return {
    noteStep: noteStep.toUpperCase(),
    noteAlter: noteAlter,
    noteOctave: noteOctave,
    clefSign: clefSign.toUpperCase(),
    clefLine: clefLine,
    clefOctaveChange: coc,
  }
}
