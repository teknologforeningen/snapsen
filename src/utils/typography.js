import { type } from "../styles/Style"
import Typography from "typography"
const typography = new Typography({
  title: "Snapsen",
  baseFontSize: type.baseFontSize,
  baseLineHeight: 1.4,
  headerFontFamily: type.headerFontFamily,
  bodyFontFamily: type.bodyFontFamily,
  scaleRatio: type.scaleRatio,
  googleFonts: type.googleFonts,
  overrideStyles: ({ ajustFontSizeTo, scale, rythm }, options) => ({
    body: {
      "background-color": "var(--bg)",
    },
    a: {
      color: "var(--textLink)",
    },
    "h1,h2,h4": {
      marginBottom: rhythm(1 / 2),
    },
  }),
})

// Export helper functions
export const { scale, rhythm, options } = typography
export default typography
