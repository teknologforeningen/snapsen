import { render } from "@testing-library/react"
import React from "react"
import "@testing-library/jest-dom/extend-expect"
import {
  getNoteNodeName,
  getNoteNodeParams,
  getNoteDistance,
  noteShorthand2Note,
  noteShorthand2NoteAlter,
  noteShorthand2NoteOctave,
} from "../noteUtils"
import { Note } from "../../types/song"

interface testNote {
  note: Note
  shorthand: string
  nodeName: string
}

const testNotes: testNote[] = [
  {
    note: {
      noteStep: "C",
      noteAlter: 0,
      noteOctave: 4,
      clefSign: "G",
      clefLine: 2,
      clefOctaveChange: -1,
    },
    shorthand: "c1",
    nodeName: "singleNoteMxl_C_0_4_G_2_-1",
  },
  {
    note: {
      noteStep: "D",
      noteAlter: 0,
      noteOctave: 4,
      clefSign: "G",
      clefLine: 2,
      clefOctaveChange: -1,
    },
    shorthand: "d1",
    nodeName: "singleNoteMxl_D_0_4_G_2_-1",
  },
  {
    note: {
      noteStep: "F",
      noteAlter: 0,
      noteOctave: 7,
      clefSign: "G",
      clefLine: 2,
      clefOctaveChange: 0,
    },
    shorthand: "f4",
    nodeName: "singleNoteMxl_F_0_7_G_2_0",
  },
  {
    note: {
      noteStep: "E",
      noteAlter: 2,
      noteOctave: 5,
      clefSign: "G",
      clefLine: 2,
      clefOctaveChange: 0,
    },
    shorthand: "e##2",
    nodeName: "singleNoteMxl_E_2_5_G_2_0",
  },
]

describe("Test function noteShorthand2NoteOctave", () => {
  test("Test if output matches expected result", () => {
    type note = {
      sh: string
      no: number
    }
    const notes: note[] = [
      { sh: "c2", no: 5 },
      { sh: "f##2", no: 5 },
      { sh: "c1", no: 4 },
      { sh: "gbb1", no: 4 },
      { sh: "c", no: 3 },
      { sh: "a#", no: 3 },
      { sh: "C", no: 2 },
      { sh: "Cb", no: 2 },
      { sh: "C1", no: 1 },
      { sh: "Cbb1", no: 1 },
      { sh: "C2", no: 0 },
      { sh: "C##2", no: 0 },
    ]
    notes.forEach((n) => {
      expect(n.no).toBe(noteShorthand2NoteOctave(n.sh))
    })
    testNotes.forEach((testNote) => {
      const shorthand = testNote.shorthand
      const note = testNote.note
      expect(noteShorthand2NoteOctave(shorthand)).toStrictEqual(note.noteOctave)
    })
  })
})

describe("Test function noteShorthand2NoteAlter", () => {
  test("Test if output matches expected result", () => {
    type note = { sh: string; alt: number }
    const notes: note[] = [
      { sh: "c2", alt: 0 },
      { sh: "f##2", alt: 2 },
      { sh: "c1", alt: 0 },
      { sh: "gbb1", alt: -2 },
      { sh: "c", alt: 0 },
      { sh: "a#", alt: 1 },
      { sh: "C", alt: 0 },
      { sh: "Cb", alt: -1 },
      { sh: "C1", alt: 0 },
      { sh: "Cbb1", alt: -2 },
      { sh: "C2", alt: 0 },
      { sh: "C##2", alt: 2 },
      { sh: "C#b#b#2", alt: 1 },
    ]
    notes.forEach((n) => {
      expect(noteShorthand2NoteAlter(n.sh)).toBe(n.alt)
    })
    testNotes.forEach((testNote) => {
      const shorthand = testNote.shorthand
      const alter = testNote.note.noteAlter
      expect(noteShorthand2NoteAlter(shorthand)).toStrictEqual(alter)
    })
  })
})
describe("Test function getNoteNodeName", () => {
  test("Test if output matches expected result", () => {
    testNotes.forEach((testNote) => {
      const nodeName = testNote.nodeName
      const note = testNote.note
      expect(nodeName).toStrictEqual(getNoteNodeName(note))
    })
  })
})

describe("Test function noteShorthand2Note", () => {
  test("Test if output matches expected result", () => {
    testNotes.forEach((testNote) => {
      const shorthand = testNote.shorthand
      const note = noteShorthand2Note(shorthand)
      expect(note).toStrictEqual(testNote.note)
    })
  })
  test("Manually change clef", () => {
    testNotes.forEach((testNote) => {
      const shorthand = testNote.shorthand
      const note = noteShorthand2Note(shorthand, "C", 3, 0)
      expect(note).toStrictEqual({
        ...testNote.note,
        clefSign: "C",
        clefLine: 3,
        clefOctaveChange: 0,
      })
    })
  })
})

describe("Test function getNoteNodeParams", () => {
  test("Test if output matches expected", () => {
    testNotes.forEach((testNote) => {
      expect(getNoteNodeParams(testNote.nodeName)).toStrictEqual(testNote.note)
    })
  })
})

describe("Test conversion funcs in relation to each other", () => {
  test("Test if chaining getNoteNodeParams and getNoteNodeName return expected result", () => {
    testNotes.forEach((testNote) => {
      const nodeName: string = testNote.nodeName
      const convNodeName: string = getNoteNodeName(
        getNoteNodeParams(getNoteNodeName(getNoteNodeParams(testNote.nodeName)))
      )
      expect(nodeName).toStrictEqual(convNodeName)
    })
  })
})

describe("Test function getNoteDistance", () => {
  test("Test if getNoteDistance returns expected result", () => {
    const testNotes = [
      {
        ref: { noteStep: "C", noteAlter: 0, noteOctave: 4 },
        notesWithDist: [
          // Chromatic
          [{ noteStep: "C", noteAlter: -1, noteOctave: 4 }, -1],
          [{ noteStep: "C", noteAlter: 0, noteOctave: 4 }, 0],
          [{ noteStep: "C", noteAlter: 1, noteOctave: 4 }, 1],
          [{ noteStep: "D", noteAlter: -1, noteOctave: 4 }, 1],
          [{ noteStep: "D", noteAlter: 0, noteOctave: 4 }, 2],
          [{ noteStep: "D", noteAlter: 1, noteOctave: 4 }, 3],
          [{ noteStep: "E", noteAlter: -1, noteOctave: 4 }, 3],
          [{ noteStep: "E", noteAlter: 0, noteOctave: 4 }, 4],
          [{ noteStep: "E", noteAlter: 1, noteOctave: 4 }, 5],
          [{ noteStep: "F", noteAlter: -1, noteOctave: 4 }, 4],
          [{ noteStep: "F", noteAlter: 0, noteOctave: 4 }, 5],
          [{ noteStep: "F", noteAlter: 1, noteOctave: 4 }, 6],
          // Same step and octave but different alter
          ...[
            -100, -10, -1, 0, 1, 10, 100, 23, 24, 55, 23.5, 1.25, 1.333333,
          ].map((alt) => {
            return [{ noteStep: "C", noteAlter: alt, noteOctave: 4 }, alt]
          }),
        ],
      },
    ]

    expect(
      getNoteDistance(
        { noteStep: "D", noteAlter: 0, noteOctave: 1 },
        { noteStep: "D", noteAlter: 0, noteOctave: 4 }
      )
    ).toBeCloseTo(36)
    expect(
      getNoteDistance(
        { noteStep: "D", noteAlter: 0, noteOctave: 1 },
        { noteStep: "E", noteAlter: 0, noteOctave: 4 }
      )
    ).toBeCloseTo(38)
    expect(
      getNoteDistance(
        { noteStep: "F", noteAlter: 0, noteOctave: 1 },
        { noteStep: "E", noteAlter: 0, noteOctave: 4 }
      )
    ).toBeCloseTo(35)
    expect(
      getNoteDistance(
        { noteStep: "F", noteAlter: 35, noteOctave: 1 },
        { noteStep: "E", noteAlter: 0, noteOctave: 4 }
      )
    ).toBeCloseTo(0)
    expect(
      getNoteDistance(
        { noteStep: "B", noteAlter: 0, noteOctave: 4 },
        { noteStep: "B", noteAlter: 0, noteOctave: 1 }
      )
    ).toBeCloseTo(-36)
    expect(
      getNoteDistance(
        { noteStep: "B", noteAlter: 0, noteOctave: 7 },
        { noteStep: "C", noteAlter: 0, noteOctave: 4 }
      )
    ).toBeCloseTo(-47)
    expect(
      getNoteDistance(
        { noteStep: "F", noteAlter: 0, noteOctave: 1 },
        { noteStep: "E", noteAlter: 0, noteOctave: -1 }
      )
    ).toBeCloseTo(-25)

    testNotes.forEach((n) => {
      n.notesWithDist.forEach((nwd) => {
        expect(getNoteDistance(n.ref, nwd[0] as Note)).toBeCloseTo(
          nwd[1] as number
        )
      })
    })
  })
})
