// @ts-nocheck
import a from "./a.wav"
import ab from "./ab.wav"
import abb from "./g.wav"
import ai from "./ai.wav"
import aii from "./b.wav"
import b from "./b.wav"
import bb from "./bb.wav"
import bbb from "./a.wav"
import bi from "./c.wav"
import bii from "./ci.wav"
import c from "./c.wav"
import cb from "./b.wav"
import cbb from "./bb.wav"
import ci from "./ci.wav"
import cii from "./d.wav"
import d from "./d.wav"
import db from "./db.wav"
import dbb from "./c.wav"
import di from "./di.wav"
import dii from "./e.wav"
import e from "./e.wav"
import eb from "./eb.wav"
import ebb from "./d.wav"
import ei from "./f.wav"
import eii from "./fi.wav"
import f from "./f.wav"
import fb from "./e.wav"
import fbb from "./eb.wav"
import fi from "./fi.wav"
import fii from "./g.wav"
import g from "./g.wav"
import gb from "./gb.wav"
import gbb from "./f.wav"
import gi from "./ab.wav"
import gii from "./a.wav"

export {
  c,
  ci,
  cii,
  db,
  dbb,
  d,
  di,
  dii,
  eb,
  ebb,
  e,
  ei,
  eii,
  fb,
  fbb,
  f,
  fi,
  fii,
  gb,
  gbb,
  g,
  gi,
  gii,
  ab,
  abb,
  a,
  ai,
  aii,
  bb,
  bbb,
  b,
  bi,
  bii,
  cb,
  cbb,
}
