import * as React from "react"
import SEO from "../components/Layout/SEO"
import { PageHeaderRow } from "../styles/SongDetails"
import { Col } from "react-flexbox-grid"
import { graphql } from "gatsby"
import { Trans } from "gatsby-plugin-react-i18next"

const NotFoundPage: React.FunctionComponent = () => (
  <>
    <SEO description="404: Not found" />
    <PageHeaderRow>
      <Col xs={12}>
        <h1>
          <Trans>404: Page could not be found</Trans>
        </h1>
        <p>
          <Trans>
            For some the reason the page you are looking for could not be found.
          </Trans>
        </p>
      </Col>
    </PageHeaderRow>
  </>
)

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`

export default NotFoundPage
