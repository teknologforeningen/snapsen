import { render } from "@testing-library/react"
import React from "react"
import "@testing-library/jest-dom/extend-expect"
import Wrapper from "../Layout/Wrapper"

describe("Test if the Wrapper component is working as expected", () => {
  it("<Wrapper /> matches snapshot", () => {
    const component = render(
      <Wrapper>
        <></>
      </Wrapper>
    )
    expect(component.container).toMatchSnapshot()
  })
})
