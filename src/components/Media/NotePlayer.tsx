import React, { useState, useEffect, SyntheticEvent } from "react"
import {
  NoteCardStyle,
  TuningForkCard,
  TuningForkSVGPath,
} from "../../styles/NotePlayer"
import { NoteButton, NoteSvgStyle, NoteSvgWrapper } from "../../styles/Button"
import { NotePlayerProps, SongStartnote } from "../../types/song"
import { getNoteLabel } from "../../utils/noteUtils"
import { Col } from "react-flexbox-grid"
import { Howl, SoundSpriteDefinitions } from "howler"
import { getNoteNodeName } from "../../utils/noteUtils"
import { flow } from "lodash"

const maxVol = 1
const minVol = 0.05
const fadeLen = 10

export const NotePlayer = (props: NotePlayerProps) => {
  //const [track, setTrack] = useState<Howl>(null)
  const [soundIdMap, setSoundIdMap] = useState(new Map())
  const [track, setTrack] = useState<Howl>(null)
  const updateSoundIdMap = (k: string, v: number) => {
    setSoundIdMap(new Map(soundIdMap.set(k, v)))
  }

  const createHowl = () => {
    const sprites: SoundSpriteDefinitions = props.soundSpriteDef
    const audioFiles = props?.soundSprites?.map((file) => {
      return file.publicURL
    })
    const currentNotes = !props?.notes
      ? []
      : props.notes.map((note) => {
          return getNoteNodeName({
            noteStep: note.noteStep,
            noteAlter: note.noteAlter,
            noteOctave: note.noteOctave,
          })
        })
    const currentSprites = !!!sprites
      ? {}
      : flow([
          Object.entries,
          (sprArr) =>
            sprArr.filter(([name]: [string]) => {
              // If currentNotes are defined, create sprite definitions only for these
              // notes. Otherwise create definitions for all notes.
              if (currentNotes.length > 0) {
                return currentNotes.includes(name)
              } else {
                return true
              }
            }),
          Object.fromEntries,
        ])(sprites)

    // Create new Howl from file
    const newTrack = new Howl({
      src: audioFiles as string[],
      sprite: currentSprites,
      preload: true,
      autoplay: false,
      loop: true,
      volume: maxVol,
      onplay: (soundId: number) => {
        newTrack.fade(minVol, maxVol, fadeLen, soundId)
      },
      onfade: (soundId: number) => {
        if ((newTrack.volume(soundId) as number) - 0.01 < 0) {
          newTrack.pause(soundId)
          newTrack.seek(0, soundId)
        }
      },
      onplayerror: (soundId: number, errMsg: unknown) => {
        console.error(
          `Unable to play sound with id ${soundId}, message: ${errMsg}`
        )
      },
    })
    return newTrack
  }

  // Prevent opening context menu on note buttons.
  function handleContextMenu(e: SyntheticEvent) {
    e.preventDefault()
  }

  useEffect(() => {
    // Create Howl
    setTrack(createHowl())
    return () => {
      // When component unmounts, unload the sound file
      if (!!track) {
        track.off()
        track.stop()
        track.unload()
      }
    }
  }, [])

  function handlePlay(spriteName: string) {
    return () => {
      let id = soundIdMap.get(spriteName)
      if (typeof id !== "number") {
        id = track.play(spriteName)
      } else {
        id = track.play(id)
      }
      if (typeof id !== "number") {
        console.error(`ID of soundsprite "${spriteName}" is "${id}" (NaN)`)
      }
      updateSoundIdMap(spriteName, id)
    }
  }

  function handlePause(spriteName: string) {
    return () => {
      const id = soundIdMap.get(spriteName)
      if (typeof id === "number" && !Number.isNaN(id)) {
        track.fade(maxVol, 0, fadeLen, id)
      }
    }
  }

  const firstNote: SongStartnote = props.notes[0]

  const firstNoteName = getNoteNodeName({
    ...firstNote,
    clefSign: undefined,
    clefOctaveChange: undefined,
    clefLine: undefined,
  })
  const button = props.cardStyle ? (
    <NoteCardStyle
      onMouseDown={handlePlay(firstNoteName)}
      onTouchStart={handlePlay(firstNoteName)}
      onMouseLeave={handlePause(firstNoteName)}
      onTouchCancel={handlePause(firstNoteName)}
      onMouseUp={handlePause(firstNoteName)}
      onTouchEnd={handlePause(firstNoteName)}
      className={props.className}
      id={`notePlayer-${firstNote}`}
    >
      <TuningForkCard viewBox={"0 0 15 100"}>
        <TuningForkSVGPath />
      </TuningForkCard>
    </NoteCardStyle>
  ) : (
    <>
      {props?.notes?.map((note: SongStartnote, noteIndex: number) => {
        const soundName = getNoteNodeName({
          noteStep: note.noteStep,
          noteAlter: note.noteAlter,
          noteOctave: note.noteOctave,
        })
        return (
          <Col xs={12} sm={6} xl={4} key={`note${noteIndex}`}>
            <NoteButton
              onMouseDown={handlePlay(soundName)}
              onTouchStart={handlePlay(soundName)}
              onMouseLeave={handlePause(soundName)}
              onTouchCancel={handlePause(soundName)}
              onMouseUp={handlePause(soundName)}
              onTouchEnd={handlePause(soundName)}
              className={props.className}
              id={`notePlayer${noteIndex}`}
            >
              {props?.text && props.text}
              <NoteSvgWrapper>
                <NoteSvgStyle
                  className={"noselect"}
                  draggable={false}
                  src={note?.svgFile?.publicURL}
                  onContextMenu={handleContextMenu}
                />
              </NoteSvgWrapper>
              {getNoteLabel(note)}
            </NoteButton>
          </Col>
        )
      })}
    </>
  )
  return button
}
