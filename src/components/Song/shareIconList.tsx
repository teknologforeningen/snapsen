import React from "react"

import { ShareIconItem } from "../../types/share"
import { Facebook, Twitter, Linkedin, Mail, Send, Copy } from "react-feather"
import { buttonProps } from "../../styles/FeatherIcon"
import { Trans } from "gatsby-plugin-react-i18next"
export interface IconListObject {
  [key: string]: ShareIconItem
}

const externalOpen = (URL: string) => window.open(URL, "_blank", "noopener")
const copyLink = (URL: string) => {
  navigator.clipboard
    .writeText(decodeURIComponent(URL))
    .then(() => {
      console.log("Link copied to clipboard.")
    })
    .catch(() => {
      alert("Error copying link to clipboard.")
    })
}

export const shareIconList: IconListObject = {
  facebook: {
    path: <Facebook {...buttonProps} />,
    color: "#0076FB",
    title: "Facebook",
    externalOpen: (l) =>
      externalOpen(`https://www.facebook.com/sharer/sharer.php?u=${l}`),
  },
  twitter: {
    path: <Twitter {...buttonProps} />,
    color: "#1DA1F2",
    title: "Twitter",
    externalOpen: (l, t) =>
      externalOpen(`https://twitter.com/intent/tweet?text=${t}&url=${l}`),
  },
  whatsapp: {
    path: (
      <path d="M17.5 14.4l-2-1c-.3 0-.5-.1-.7.2l-1 1.1c-.1.2-.3.3-.6.1s-1.3-.5-2.4-1.5a9 9 0 01-1.7-2c-.1-.3 0-.5.2-.6l.4-.6c.2-.1.2-.3.3-.5v-.5L9 7c-.2-.6-.4-.5-.6-.5h-.6c-.2 0-.5 0-.8.4-.2.3-1 1-1 2.5s1 2.8 1.2 3c.2.2 2.1 3.2 5.1 4.5l1.7.6a4 4 0 001.9.2c.5-.1 1.7-.8 2-1.5.2-.6.2-1.2.1-1.4l-.5-.3M12 21.8a9.9 9.9 0 01-5-1.4l-.4-.2-3.7 1 1-3.7-.2-.3a9.9 9.9 0 01-1.5-5.3 9.9 9.9 0 0116.8-7 9.8 9.8 0 013 7 9.9 9.9 0 01-10 9.9m8.4-18.3A11.8 11.8 0 0012.1 0 12 12 0 001.8 17.8L0 24l6.4-1.6a11.9 11.9 0 005.6 1.4 12 12 0 0012-11.9 11.8 11.8 0 00-3.5-8.4z" />
    ),
    color: "#25D366",
    title: "WhatsApp",
    externalOpen: (l, t) =>
      externalOpen(`https://api.whatsapp.com/send?text=${t} ${l}`),
  },
  reddit: {
    path: (
      <path d="M12 0A12 12 0 000 12a12 12 0 0012 12 12 12 0 0012-12A12 12 0 0012 0zm5.01 4.74c.69 0 1.25.56 1.25 1.25a1.25 1.25 0 01-2.5.06l-2.6-.55-.8 3.75c1.83.07 3.48.63 4.68 1.49.3-.31.73-.5 1.2-.5.97 0 1.76.8 1.76 1.76 0 .72-.43 1.33-1.01 1.61a3.11 3.11 0 01.04.52c0 2.7-3.13 4.87-7 4.87-3.88 0-7-2.17-7-4.87 0-.18 0-.36.04-.53A1.75 1.75 0 014.03 12a1.75 1.75 0 012.96-1.26 8.52 8.52 0 014.74-1.5l.89-4.17a.34.34 0 01.14-.2.35.35 0 01.24-.04l2.9.62a1.21 1.21 0 011.11-.7zM9.25 12a1.25 1.25 0 101.25 1.25c0-.69-.56-1.25-1.25-1.25zm5.5 0a1.25 1.25 0 000 2.5 1.25 1.25 0 000-2.5zm-5.47 3.99a.33.33 0 00-.23.1.33.33 0 000 .46c.84.84 2.49.91 2.96.91.48 0 2.1-.06 2.96-.91a.36.36 0 00.03-.47.33.33 0 00-.46 0c-.55.54-1.68.73-2.51.73-.83 0-1.98-.2-2.51-.73a.33.33 0 00-.24-.1z" />
    ),
    color: "#FF4500",
    title: "Reddit",
    externalOpen: (l, t) =>
      externalOpen(`https://www.reddit.com/submit?url=${l}&title=${t}`),
  },
  telegram: {
    path: <Send {...buttonProps} />,
    color: "#0088CC",
    title: "Telegram",
    externalOpen: (l, t) =>
      externalOpen(`https://telegram.me/share/msg?url=${l}&text=${t}`),
  },
  linkedin: {
    path: <Linkedin {...buttonProps} />,
    color: "#0073b1",
    title: "LinkedIn",
    externalOpen: (l, t, ti) =>
      externalOpen(
        `https://www.linkedin.com/shareArticle?mini=true&url=${l}&title=${ti}&summary=${t}`
      ),
  },
  email: {
    path: <Mail {...buttonProps} />,
    color: "#E53E3E",
    title: "Email",
    externalOpen: (l, t) => externalOpen(`mailto:?body=${l}&subject=${t}`),
  },
  copy: {
    path: <Copy {...buttonProps} />,
    color: "#718096",
    title: <Trans>Copy link</Trans>,
    externalOpen: (l) => copyLink(l),
  },
  vk: {
    path: (
      <path d="M22.7 6.24c.16-.52 0-.9-.73-.9H19.5c-.62 0-.91.32-1.06.69 0 0-1.25 3.04-3.02 5.02-.57.57-.83.75-1.15.75-.15 0-.39-.18-.39-.7V6.24c0-.63-.17-.9-.69-.9H9.34c-.39 0-.62.29-.62.56 0 .59.88.72.97 2.4v3.6c0 .8-.14.95-.45.95-.84 0-2.86-3.06-4.06-6.56-.24-.68-.48-.96-1.1-.96H1.61c-.7 0-.85.33-.85.7 0 .65.84 3.88 3.88 8.14 2.03 2.92 4.9 4.5 7.5 4.5 1.55 0 1.75-.35 1.75-.96 0-2.78-.15-3.04.64-3.04.36 0 .99.18 2.44 1.58 1.67 1.67 1.95 2.42 2.88 2.42h2.45c.7 0 1.06-.35.85-1.05-.46-1.45-3.62-4.44-3.76-4.64-.36-.47-.26-.68 0-1.1 0 0 3-4.21 3.3-5.64Z" />
    ),
    color: "#07f",
    title: "VK",
    externalOpen: (l, t, ti) =>
      externalOpen(`http://vk.com/share.php?url=${l}&title=${ti}&comment=${t}`),
  },
  okru: {
    path: (
      <path d="M13.6 16.4c1-.3 2-.7 2.8-1.2a1.5 1.5 0 0 0-1.5-2.5 6.3 6.3 0 0 1-6.7 0 1.5 1.5 0 1 0-1.5 2.5c.9.5 1.8 1 2.9 1.2L6.8 19a1.4 1.4 0 1 0 2 2l2.8-2.6 2.7 2.7a1.5 1.5 0 1 0 2-2l-2.7-2.8Zm-2-11.1a2 2 0 1 1 0 4.1 2 2 0 0 1 0-4.1Zm0 7a5 5 0 1 0 0-10 5 5 0 0 0 0 10Z" />
    ),
    color: "#e27e35",
    title: "Okru",
    externalOpen: (l) =>
      externalOpen(
        `https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=${l}`
      ),
  },
}
