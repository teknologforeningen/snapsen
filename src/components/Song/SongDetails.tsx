import React from "react"
import { NotePlayer } from "../Media/NotePlayer"
import { HowlerPlayer } from "../Media/HowlerPlayer"
import slugify from "underscore.string/slugify"
import Collapsible from "react-collapsible"
import { Col, Row } from "react-flexbox-grid"
import { SongTags } from "./SongTags"
import { ShareButton } from "./SongShare"
import { SongDetailsProps, SongStartnote } from "../../types/song"
import SheetMusicViewer from "../Media/SheetMusicViewer"
import { ShareData } from "../../types/share"
import { useTranslation } from "gatsby-plugin-react-i18next"

import {
  Lyrics,
  TopCreditsStyle,
  BottomCreditsStyle,
  PageHeaderRow,
} from "../../styles/SongDetails"

const TopCredits = ({ melody, pre }) => {
  const { t } = useTranslation()

  return (
    <TopCreditsStyle>
      {melody && (
        <>
          {t("melody")}: {melody}
          <br />
        </>
      )}
      {pre && <div>{pre}</div>}
    </TopCreditsStyle>
  )
}

export const rightAlignedButtonProps = {
  xs: 12,
  sm: 5,
  md: 4,
  lg: 3,
}

const SheetCollapsible = ({ smallSheet, mediumSheet, largeSheet }) => {
  const { t } = useTranslation()
  return (
    <Collapsible
      transitionTime={200}
      easing={"ease-in"}
      trigger={t("sheet music")}
      triggerElementProps={{ id: "collapsible-sheet" }}
      contentElementId="collapsible-sheet-content"
    >
      <SheetMusicViewer
        smallUrl={smallSheet}
        mediumUrl={mediumSheet}
        largeUrl={largeSheet}
      ></SheetMusicViewer>
    </Collapsible>
  )
}
const BottomCredits = ({ melodyCredit, lyricsCredit }) => {
  const { t } = useTranslation()
  return (
    <BottomCreditsStyle>
      {melodyCredit && (
        <>
          {t("music")}: {melodyCredit}
          <br />
        </>
      )}
      {lyricsCredit && (
        <>
          {t("lyrics")}: {lyricsCredit}
          <br />
        </>
      )}
    </BottomCreditsStyle>
  )
}

const SongDetails: React.FunctionComponent<SongDetailsProps> = ({
  title,
  lyrics,
  lyricsCredit,
  melody,
  melodyCredit,
  pre,
  largeSheet,
  mediumSheet,
  smallSheet,
  audio,
  tag,
  sheetOnly,
  url,
  songStartnotes,
  toneAuSpMeta,
  toneAuSp,
}) => {
  const hasSheets = smallSheet && mediumSheet && largeSheet
  //TODO: Make text customizable
  const shareData: ShareData = {
    url: url,
    title: title,
    text: title + " – Snapsen",
  }

  const { t } = useTranslation()
  const shareLabel = t("Share link")
  const shareDesc = t("Share a link to this song via:")
  const hasSingleStartNote = songStartnotes && songStartnotes.length === 1
  const hasManyStartNotes = songStartnotes && songStartnotes.length >= 2

  return (
    <>
      <PageHeaderRow id="song-header" middle="xs">
        <Col xs={12} sm md lg>
          <h1>{title}</h1>
          <SongTags tag={tag} />
          <TopCredits melody={melody} pre={pre} />
        </Col>

        {hasSingleStartNote && (
          <NotePlayer
            className={`noselect`}
            notes={[songStartnotes[0]]}
            cardStyle={false}
            soundSprites={toneAuSp}
            soundSpriteDef={toneAuSpMeta}
          />
        )}
      </PageHeaderRow>
      {hasManyStartNotes && (
        <Collapsible
          transitionTime={200}
          easing={"ease-in"}
          trigger={t("starting tones")}
          triggerElementProps={{ id: "collapsible-tones" }}
          contentElementId="collapsible-tones-content"
        >
          <Row>
            <NotePlayer
              className={`noselect`}
              notes={songStartnotes}
              cardStyle={false}
              soundSprites={toneAuSp}
              soundSpriteDef={toneAuSpMeta}
            />
          </Row>
        </Collapsible>
      )}
      {hasSheets && !sheetOnly && (
        <SheetCollapsible
          smallSheet={smallSheet}
          mediumSheet={mediumSheet}
          largeSheet={largeSheet}
        />
      )}
      {audio &&
        audio.map((singleAudio) => {
          if (singleAudio) {
            let audioKey = "unknown-audio"
            if ("name" in singleAudio) {
              audioKey = slugify(singleAudio["name"])
            } else if ("file" in singleAudio) {
              audioKey = slugify(singleAudio["file"])
            }

            return (
              <HowlerPlayer audio={singleAudio} key={`audio-${audioKey}`} />
            )
          }
        })}
      {!sheetOnly || (sheetOnly && !hasSheets) ? (
        <Lyrics id="song-lyrics" dangerouslySetInnerHTML={{ __html: lyrics }} />
      ) : (
        <SheetMusicViewer
          smallUrl={smallSheet}
          mediumUrl={mediumSheet}
          largeUrl={largeSheet}
        ></SheetMusicViewer>
      )}
      <BottomCredits melodyCredit={melodyCredit} lyricsCredit={lyricsCredit} />
      <Row end="xs">
        <Col {...rightAlignedButtonProps}>
          <ShareButton
            data={shareData}
            label={shareLabel}
            description={shareDesc}
          />
        </Col>
      </Row>
    </>
  )
}

export default SongDetails
