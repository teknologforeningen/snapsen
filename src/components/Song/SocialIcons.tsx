import React from "react"

import { SocialIconsProps, ShareIconProps } from "../../types/share"
import { shareIconList } from "./shareIconList"
import { Button } from "../../styles/Button"
import { ButtonIcon } from "../../styles/FeatherIcon"
import { globalTransition } from "../../styles/Style"
import styled from "styled-components"
import { Row, Col } from "react-flexbox-grid"

const ShareIconStyle = styled.svg`
  fill: var(--fg);
  transition:
    color ${globalTransition},
    background-color ${globalTransition};
  width: 1.5em;
  height: 1.5em;
`

export default function ShareIcon({
  name,
  data,
  onClick,
  onClose,
}: ShareIconProps) {
  const { path, externalOpen, title } = shareIconList[name]

  const handleOnButtonClicked = () => {
    onClick && onClick(name) // callback
    externalOpen(encodeURIComponent(data.url), data.text, data.title)
    onClose()
  }

  return (
    <Button onClick={handleOnButtonClicked} aria-label={name}>
      {title}
      <ButtonIcon>
        <ShareIconStyle viewBox="0 0 24 24">{path}</ShareIconStyle>
      </ButtonIcon>
    </Button>
  )
}
export const SocialIcons = ({
  sites,
  data,
  onClose,
  onClick,
}: SocialIconsProps) => (
  <section role="dialog" aria-modal="true">
    <Row>
      {sites.map((name) => (
        <Col xs={12} sm={6} md={4} key={name}>
          <ShareIcon
            name={name}
            data={data}
            onClose={onClose}
            onClick={onClick}
          />
        </Col>
      ))}
    </Row>
  </section>
)
