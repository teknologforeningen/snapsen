import React, { useCallback, useState, useMemo } from "react"
import { Button } from "../../styles/Button"
import { Share2, X } from "react-feather"
import { buttonProps, ButtonIcon } from "../../styles/FeatherIcon"
import ReactModal from "react-modal"
import { ComponentType } from "react"
import { ShareModalStyle } from "../../styles/SongDetails"
import { rightAlignedButtonProps } from "./SongDetails"
import { Row, Col, Grid } from "react-flexbox-grid"
import { SocialIcons } from "./SocialIcons"
//import { shareIconList } from "./shareIconList"
import { ShareButtonProps } from "../../types/share"
import { Trans, useTranslation } from "gatsby-plugin-react-i18next"

//const defaultSites = Object.keys(shareIconList)
//TODO: Add option to config.yml
const defaultSites = [
  "facebook",
  "twitter",
  "telegram",
  "reddit",
  "whatsapp",
  "email",
  "copy",
]

export const ShareButton = ({ label, data, description }: ShareButtonProps) => {
  const [showModal, setShowModal] = useState(false)
  const shareData = useMemo(
    () => ({
      ...data,
      title: data.title || "share",
      text: data.text || "",
      url:
        data.url ||
        (typeof window !== "undefined" && window.location.href) ||
        "",
    }),
    [data]
  )

  const handleSharing = useCallback(async () => {
    if (navigator.share) {
      try {
        await navigator.share(shareData)
      } catch (error) {
        console.warn(
          `Could not share link using Web Share API due to error: ${error}`
        )
      }
    } else {
      // fallback code
      setShowModal(true)
    }
  }, [shareData])

  ReactModal.setAppElement("#___gatsby")
  ReactModal.defaultStyles = ShareModalStyle
  const TypeSafeModal = ReactModal as ComponentType<ReactModal["props"]>
  const onRequestClose = (event?: React.MouseEvent | React.KeyboardEvent) => {
    setShowModal(false)
  }
  const onIconClick = (name: string) => {
    const msg = "Shared song to " + name
    console.log(msg)
  }
  return (
    <>
      <Button onClick={handleSharing}>
        {label}
        <ButtonIcon>
          <Share2 id="share-button" {...buttonProps} />
        </ButtonIcon>
      </Button>
      <TypeSafeModal
        isOpen={showModal}
        contentLabel={label}
        id={"song-share-modal"}
        ariaHideApp={true}
        shouldCloseOnOverlayClick={true}
        shouldCloseOnEsc={true}
        onRequestClose={onRequestClose}
      >
        <Grid>
          <Row>
            <Col xs={12} md={10} mdOffset={1}>
              <Row>
                <Col xs={12}>
                  <h1>{label}</h1>
                  {description && <p>{description}</p>}
                  <SocialIcons
                    sites={defaultSites}
                    data={shareData}
                    onClose={onRequestClose}
                    onClick={onIconClick}
                  />
                </Col>
              </Row>
              <hr />
              <Row end="xs">
                <Col {...rightAlignedButtonProps}>
                  <Button onClick={onRequestClose}>
                    <Trans>Close</Trans>
                    <ButtonIcon>
                      <X {...buttonProps} />
                    </ButtonIcon>
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </TypeSafeModal>
    </>
  )
}
