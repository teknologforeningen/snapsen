import React, { FunctionComponent } from "react"
import SongList from "../Song/SongList"
import styled from "styled-components"
import { useSearch } from "./SearchContext"
import { useLoadingSearch } from "../../utils/useLoadingSearch"
import { useTranslation } from "gatsby-plugin-react-i18next"
const SearchHintStyle = styled.div`
  margin-top: 0;
`

/**
 * If search is active, displays search results.
 * Otherwise renders child components.
 */
const SearchResults: FunctionComponent = () => {
  const { t } = useTranslation()
  const strTitle = t("Search")
  const strSearchHint = t(
    "Search songs by title, lyrics, page number, or categories."
  )
  const strSearchFailed = t("No songs found.")

  const { searching, focused } = useSearch()
  const { songs, loadMore, hasMore } = useLoadingSearch()
  if (focused && !searching) {
    return (
      <SearchHintStyle>
        <>
          <h3>{strTitle}</h3>
          {strSearchHint}
        </>
      </SearchHintStyle>
    )
  } else if (searching && songs.length === 0) {
    return (
      <SearchHintStyle>
        <>
          <h3>{strTitle}</h3>
          {strSearchFailed}
        </>
      </SearchHintStyle>
    )
  } else {
    return <SongList songs={songs} loadMore={loadMore} hasMore={hasMore} />
  }
}

export default SearchResults
