/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { posix } from "path"

interface SEOProps {
  children?: any
  title?: string
  description?: string
  keywords?: string[]
  author?: string
  pathname?: string
}

/**
 * Helper component for page metadata
 */
const SEO: React.FunctionComponent<SEOProps> = ({
  title,
  description,
  author,
  keywords,
  pathname,
  children,
}) => {
  const lang = `en`
  const { site } = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          siteUrl
          siteName
          description
          author
          keywords
        }
      }
    }
  `)

  const {
    title: defaultTitle,
    description: defaultDescription,
    author: defaultAuthor,
    keywords: defaultKeywords,
    siteUrl,
    siteName,
  } = site.siteMetadata

  const seo = {
    title: title || defaultTitle,
    description: description || defaultDescription,
    author: author || defaultAuthor,
    keywords: keywords || defaultKeywords,
    url: pathname ? new URL(pathname, siteUrl).toString() : siteUrl,
    siteName: siteName,
    //image: `${siteUrl}${image}`,
  }
  return (
    <>
      <title>{seo.title}</title>
      <link rel="canonical" href={seo.url} />
      <meta name={"description"} content={seo.description} />
      <meta property={"og:title"} content={seo.title} />
      <meta property={"og:description"} content={seo.description} />
      <meta property={"og:site_name"} content={seo.siteName} />
      <meta id={"og_type"} property={"og:type"} content={"website"} />
      <meta name={"twitter:card"} content={"summary"} />
      <meta name={"twitter:creator"} content={seo.author} />
      <meta name={"twitter:title"} content={seo.title} />
      <meta name={"twitter:description"} content={seo.description} />
      <meta
        name="viewport"
        content={
          "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
        }
      />
      <meta name="keywords" content={seo.keywords.join(", ")} />
      {children}
    </>
  )
}

export default SEO
