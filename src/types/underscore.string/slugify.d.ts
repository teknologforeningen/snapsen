declare module "underscore.string/slugify" {
    const slugify: (str: string) => string;
    export default slugify;
}
