// import original module declarations
import "styled-components"
import {
  SnapsenColorConf,
  SnapsenColorFilterConf,
  SnapsenTagLabelMap,
} from "./snapsenConfig"

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme {
    colorsLight: SnapsenColorConf
    colorsDark: SnapsenColorConf
    colorsLightFilters: SnapsenColorFilterConf
    colorsDarkFilters: SnapsenColorFilterConf
    typeLabels: SnapsenTagLabelMap[]
  }
}
