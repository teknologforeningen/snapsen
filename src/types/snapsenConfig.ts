export interface SnapsenMeta {
  title: string
  description: string
  siteUrl: string
  siteName: string
  author: string
  keywords: string[]
  appIconPath: string
  iconPath: string
  colorsLight: SnapsenColorConf
  colorsDark: SnapsenColorConf
  colorsLightFilters: SnapsenColorFilterConf
  colorsDarkFilters: SnapsenColorFilterConf
  breakpoints: SnapsenBreakpoints
  tags: SnapsenTag[]
  songListLoadThreshold: number
  songListSearchLimit: number
  defaultType: SnapsenTagLabelMap
  useCategoryIcons: boolean
  sheetOnly: boolean
  languages: string[]
  defaultLanguage: string
  redirectLanguage: boolean
}

export interface SnapsenConfig extends SnapsenMeta {
  books: Book[]
}

interface GitBook {
  source: "git"
  name: string
  remote: string
  branch?: string
  patterns?: string[]
}

interface FileSystemBook {
  source: "filesystem"
  name: string
  path: string
  ignore?: string[]
}

export type Book = GitBook | FileSystemBook

export interface SnapsenColorConf {
  fg: string
  bg: string
  neutral: string
  primary: string
  alert: string
  accent1: string
  accent2: string
  shadeFactor: number
}

export interface SnapsenColorFilterConf {
  fg: string
}

interface SnapsenBreakpoints {
  xs: number
  sm: number
  md: number
  lg: number
}

export interface SnapsenTag {
  value: string
  label: string
  color: string
  colorFilter?: string
  darkColorFilter?: string
  labelMap?: SnapsenTagLabelMap[]
  icon?: string
}

export interface SnapsenTagLabelMap {
  value: string
  label: string
  shortLabel: string
  icon?: string
  color?: string
  darkColor?: string
  colorFilter?: string
  darkColorFilter?: string
}
