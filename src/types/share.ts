export interface ShareData {
  text?: string
  title?: string
  url?: string
}

export interface ShareButtonProps {
  label: string
  data: ShareData
  description?: string
}

export interface SocialIconsProps {
  closeText?: string
  sites: string[]
  data: Required<ShareData>
  onClick?: (name?: string) => void
  onClose: () => void
}

export interface ShareIconProps {
  name: string
  data: Required<ShareData>
  onClick?: (name?: string) => void
  onClose: () => void
}

export interface ShareIconItem {
  path: JSX.Element
  externalOpen: (l: string, t?: string, ti?: string) => void
  color: string
  viewBox?: string
  title?: string | JSX.Element
}
