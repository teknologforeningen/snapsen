import { Book } from "./book"
import { MultiValue } from "react-select"
import { FileSystemNode } from "gatsby-source-filesystem"
import { SoundSpriteDefinitions } from "howler"

export interface SongQuery {
  nodes: Song[]
}

export interface Song {
  id: string
  title: string
  slug: string
  book: Book
  tag?: SongTag[]
  page?: number[]
  html?: string
  contentFull?: string
  body?: string
  rawBody?: string
  audio?: SongAudio[]
  startnote?: SongStartnote[]
  frontmatter?: {
    bookSlug?: string
    page?: number
    pre?: string
    startnote?: string[]
    lyricsCredit?: string
    melody?: string
    melodyCredit?: string
    type?: string
    musicalKey?: string
    songOrder?: string[]
  }
  smallSheet?: {
    publicURL: string
  }
  mediumSheet?: {
    publicURL: string
  }
  largeSheet?: {
    publicURL: string
  }
}

export interface IndexSong {
  id: string
  title: string
  contentFull: string
  melody?: string
  melodyCredit?: string
  lyricsCredit?: string
  tag: string[]
}

export interface SongTag {
  value: string
  tag: string
  label: string
  shortLabel: string
  color?: string
}

export interface SongTagGroup {
  label: string
  options: MultiValue<SongTag>
}

export interface SongTagLabelMap {
  value: string
  label?: string
  shortLabel?: string
  color?: string
  darkColor?: string
}

export interface SongAudio {
  name: string
  file: {
    name: string
    publicURL: string
  }
  voices?: SongAudioVoice[]
}

export interface SongAudioVoice {
  name: string
  startTime: number
  duration: number
}
export interface SongDetailsProps {
  title: string
  lyrics?: string
  lyricsCredit?: string
  melody?: string
  melodyCredit?: string
  pre?: string
  startnote?: string[]
  page?: number
  tag?: SongTag[]
  largeSheet?: string | undefined
  mediumSheet?: string | undefined
  smallSheet?: string | undefined
  sheetOnly: boolean
  audio?: SongAudio[]
  url: string
  songStartnotes: SongStartnote[]
  toneAuSpMeta: SoundSpriteDefinitions
  toneAuSp: FileSystemNode[]
}

export interface SongPlayerProps {
  audioFiles: SongAudio[]
}

export interface SongPlayerState {
  currentAudioFile: SongAudio
  nextSlide: number
  activeSlide: number
}

export interface NoteHowlProps {
  notes: SongStartnote[]
  soundSpriteDef: SoundSpriteDefinitions
  soundSprites: FileSystemNode[]
}

export interface NotePlayerProps {
  text?: string
  notes: SongStartnote[]
  className: string
  cardStyle: boolean
  soundSpriteDef: SoundSpriteDefinitions
  soundSprites: FileSystemNode[]
}

export interface Note {
  noteStep: string
  noteAlter: number
  noteOctave: number
  clefSign?: string
  clefLine?: number
  clefOctaveChange?: number
}

export interface FrontmatterNote {
  note: string
  clefSign?: string
  clefLine?: number
  clefOctaveChange?: number
}

interface SongStartNoteSvgFile {
  publicURL: string
}
export interface SongStartnote {
  name: string
  noteStep: string
  noteAlter: number
  noteOctave: number
  clefSign?: string
  clefLine?: number
  clefOctaveChange?: number
  svgFile: SongStartNoteSvgFile
}

export interface ToneAudiosprite {
  name: string
  internal: {
    content: string
  }
  childrenFile: FileSystemNode[]
}
