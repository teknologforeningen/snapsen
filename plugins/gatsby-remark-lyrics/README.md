# gatsby-remark-lyrics

Custom [remark generic directives](https://github.com/remarkjs/remark-directive)
for writing song lyrics.
