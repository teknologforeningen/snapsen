const remarkDirective = require("remark-directive")
const visit = require("unist-util-visit")

const htmlI = (text, clazz) => ({
  type: "html",
  value: `<i class=${clazz}>${text}</i>`,
})

const newline = () => ({
  type: "text",
  value: "\n",
})

const wrapper = (children) => ({
  type: "custom",
  children,
  data: {
    hName: "span", // Must be span, otherwise remark places text directives in own paragraph
    hProperties: {
      class: "directive-wrapper",
    },
  },
})

const wrap = (node, left, right) => {
  const data = node.data || (node.data = {})
  data.hName = "span" // Must be span, otherwise remark places text directives in own paragraph
  data.hProperties = {
    class: "directive-main",
  }

  const oldChildren = node.children
  node.children = []
  if (left) {
    node.children.push(htmlI(left, "directive-left"))
  }
  if (right) {
    // Find last block level descendant
    let target = oldChildren
    while (
      ["paragraph", "containerDirective"].includes(
        target[target.length - 1]?.type
      )
    ) {
      target = target[target.length - 1].children
    }
    target.push(htmlI(right, "directive-right"))
  }
  node.children.push(wrapper(oldChildren))
}

const center = (node) => {
  const data = node.data || (node.data = {})
  data.hName = "span" // Must be span, otherwise remark places text directives in own paragraph
  data.hProperties = {
    class: "directive-center",
  }
}

const deleteParagraphs = (node) => {
  if (!node.children) return
  for (let i = 0; i < node.children.length; i++) {
    const child = node.children[i]
    if (child?.type === "paragraph") {
      // Add all children of the paragraph to its parents children
      node.children.splice(i, 1, ...child.children)
      // Skip children as a paragraph cannot contain another paragraph
      i += child.children.length - 1
    } else {
      deleteParagraphs(child)
    }
    // Add newline to compensate for removal of paragraph
    if (i < node.children.length - 1) {
      i++
      node.children.splice(i, 0, newline())
    }
  }
}

const paragraphify = (node) => {
  node.type = "paragraph"
  deleteParagraphs(node) // Delete and merge all inner paragraphs
}

module.exports = ({ markdownAST, markdownNode, getNode, reporter }) => {
  visit(
    markdownAST,
    ["textDirective", "leafDirective", "containerDirective"],
    (node) => {
      switch (node.name) {
        case "repeat":
          wrap(node, "//:", "://")
          break
        case "solo":
          wrap(node, "solo:", null) // Requires intl
          break
        case "all":
          wrap(node, "alla:", null) // Requires intl
          break
        case "note":
          const noteText = node.attributes.text ? node.attributes.text : "Note:"
          wrap(node, noteText, null)
          break
        case "center":
          center(node)
          break
        case "paragraphify":
          paragraphify(node)
          break
        default:
          const dots =
            node.type === "containerDirective"
              ? ":::"
              : node.type === "leafDirective"
                ? "::"
                : ":"
          const path = getNode(markdownNode.parent).absolutePath
          const line = node.position?.start.line
          reporter.error(
            `Unknown markdown generic directive "${dots}${node.name}" at ${path}:${line}`
          )
      }
    }
  )

  return markdownAST
}

module.exports.setParserPlugins = () => [remarkDirective]
