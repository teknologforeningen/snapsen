/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

import { resolve } from "path"
import { GatsbyNode } from "gatsby"
import { Song, FrontmatterNote } from "./src/types/song"
import { noteShorthand2Note } from "./src/utils/noteUtils"

import {
  mdResolveTitle,
  mdResolveSongSlug,
  resolveTags,
  resolveBook,
  resolveContentFull,
  sheetResolver,
  audioResolver,
  FMAudio,
  resolveSourceInstanceName,
  resolveBookSlug,
  startNoteResolver,
} from "./gatsby/resolvers"
import { createSheetNodes } from "./gatsby/sheetNodes"
import {
  createAudiospriteNodes,
  createToneAudiospriteNodes,
} from "./gatsby/createAudiospriteNodes"
import { createSongSearch } from "./gatsby/songSearch"
import { createSingleNoteMxl } from "./gatsby/createSingleNoteMxl"
import { uniqWith, isEqual } from "lodash"
import path from "path"
import { Node } from "gatsby"

// ----- Schema customization -------------------------------------------------
//
// Create type definitions. Otherwise type cannot be inferred for optional
// fields. Exclamation mark makes the field non-nullable, i.e. if it is allowed
// to be null or not.
//
// See: https://www.gatsbyjs.com/docs/reference/graphql-data-layer/schema-customization/#creating-type-definitions
//
// Also links book nodes (from YAML) to song nodes (from MDX) by matching the
// "sourceInstanceField" on the parent nodes. This is the "name" field given to
// gatsby-source-filesystem or the name of the repo sourced by
// gatsby-source-git.
//
// TODO: Enable having many books in each source

export const createSchemaCustomization: GatsbyNode["createSchemaCustomization"] =
  ({ actions, schema }) => {
    const { createTypes } = actions

    const markdownRemarkType = schema.buildObjectType({
      name: "MarkdownRemark",
      fields: {
        book: { type: "YamlBook!", resolve: resolveBook },
        audio: { type: "[SongAudio]", resolve: audioResolver },
        startnote: { type: "[StartNote!]", resolve: startNoteResolver },
        largeSheet: { type: "File", resolve: sheetResolver("large") },
        mediumSheet: { type: "File", resolve: sheetResolver("medium") },
        smallSheet: { type: "File", resolve: sheetResolver("small") },
        frontmatter: "MdSongFrontmatter",
        title: { type: "String!", resolve: mdResolveTitle },
        slug: { type: "String!", resolve: mdResolveSongSlug },
        tag: { type: "[SongTag!]!", resolve: resolveTags },
        contentFull: { type: "String!", resolve: resolveContentFull },
        html: { type: "String" },
        excerpt: { type: "String" },
      },
      interfaces: ["Node"],
    })

    const yamlBookType = schema.buildObjectType({
      name: "YamlBook",
      fields: {
        songs: {
          type: "[MarkdownRemark!]!",
          extensions: {
            link: { by: "book.sourceInstanceName", from: "sourceInstanceName" },
          },
        },
        sourceInstanceName: {
          type: "String!",
          resolve: resolveSourceInstanceName,
        }, // Needed for book-song linking
        slug: { type: "String!", resolve: resolveBookSlug },
        title: "String!",
        description: "String",
        image: "String",
        primaryColor: "String",
        author: "YamlBookAuthor",
        versions: "YamlBookVersions",
      },
      interfaces: ["Node"],
    })

    const typeDefs = [
      markdownRemarkType,
      yamlBookType,
      `
    type YamlBookAuthor {
      name: String!
      email: String!
    }
    type YamlBookVersions {
      book: String
      snapsen: String
    }
    type MdSongFrontmatter @dontInfer {
      slug: String
      description: String
      type: String
      lyricsCredit: String
      melody: String
      melodyCredit: String
      page: [String!]
      startnote: [MdSongFrontmatterNote!]
      musicalKey: String
      pre: String
      songOrder: [String!]
      audio: [MdSongFrontmatterAudio!]
    }
    type MdSongFrontmatterAudio {
      name: String
      file: String!
      voices: [MdSongFrontmatterAudioVoice]
    }
    type MdSongFrontmatterAudioVoice {
      name: String
      file: String
    }
    type MdSongFrontmatterNote {
      note: String!,
      clefSign: String,
      clefLine: Int,
      clefOctaveChange: Int
    }
    type SongTag {
      tag: String!,
      tagLabel: String!,
      value: String!,
      label: String!,
      shortLabel: String!,
      color: String!
    }
    type SongAudio {
      name: String
      file: File!
      voices: [SongAudioVoice!]
    }
    type SongAudioVoice {
      name: String
      startTime: Float
      duration: Float
      file: String
    }
    type StartNote {
      name: String!
      noteStep: String!
      noteAlter: Int
      noteOctave: Int!
      clefSign: String!
      clefLine: Int!
      clefOctaveChange: Int!
      svgFile: File
    }
    type SongSearch implements Node {
      index: String!
      options: String!
    }
    type SiteSiteMetadata @infer {
      title: String!
      description: String!
      siteUrl: String!
      siteName: String!
      author: String!
      keywords: [String!]!
      appIconPath: String!
      iconPath: String!
      colorsLight: SnapsenColorConf!
      colorsLightFilters: SnapsenColorFilter
      colorsDark: SnapsenColorConf!
      colorsDarkFilters: SnapsenColorFilter
      breakpoints: SnapsenBreakpoints!
      tags: [SnapsenTag!]!
      songListLoadThreshold: Int!
      songListSearchLimit: Int!
      defaultType: SnapsenTagLabelMap
      useCategoryIcons: Boolean
      sheetOnly: Boolean
    }
    type SnapsenColorConf {
      fg: String!
      bg: String!
      neutral: String!
      primary: String!
      alert: String!
      accent1: String!
      accent2: String!
      shadeFactor: Int!
    }
    type SnapsenColorFilter {
      fg: String
    }
    type SnapsenBreakpoints {
      xs: Int!
      sm: Int!
      md: Int!
      lg: Int!
    }
    type SnapsenTag {
      label: String!
      value: String!
      color: String!
      labelMap: [SnapsenTagLabelMap!]
    }
    type SnapsenTagLabelMap {
      value: String!
      label: String!
      icon: String
      color: String
      colorFilter: String
      darkColor: String
      darkColorFilter: String
      shortLabel: String!
    }
    type AudiospriteMeta {
      file: String
      name: String
      timestamps: [Float]
    }
    type AudiospriteJson implements Node {
      urls: [String]
      sprite: [AudiospriteMeta!]
      name: String
    }
    `,
    ]
    createTypes(typeDefs)
  }

// ----Create nodes------------------------------------------------------------
interface SongFrontmatter {
  audio?: FMAudio[]
  startnote?: FrontmatterNote[]
}

export const onCreateNode: GatsbyNode["onCreateNode"] = async ({
  node,
  actions,
  createNodeId,
  cache,
  reporter,
}) => {
  const { createNode, createParentChildLink } = actions

  if (node.internal.type === "File") {
    if (
      node.internal.mediaType === "application/vnd.recordare.musicxml" ||
      node.internal.mediaType === "application/vnd.recordare.musicxml+xml"
    ) {
      let singleNote = false
      const singleNoteRE = new RegExp("^(singleNoteMxl.*)$")

      if (typeof node?.base === "string" && singleNoteRE.test(node.base)) {
        reporter.verbose(
          `Snapsen -- Creating SVGs with single note settings: ${node?.base}`
        )
        singleNote = true
      } else {
        reporter.info(
          `Snapsen -- Creating SVGs from MXL with regular settings: ${node.base}`
        )
      }

      await createSheetNodes({
        fileNode: node,
        createNode,
        createNodeId,
        createParentChildLink,
        cache,
        singleNote,
      })
    }
  }

  if (node.internal.type === "MarkdownRemark") {
    const songFrontmatter = node?.frontmatter as SongFrontmatter
    const audioVoices = songFrontmatter?.audio
      ? songFrontmatter.audio.filter((audio) => {
          return "voices" in audio
        })
      : []

    if (audioVoices.length !== 0) {
      const audioSpriteActivity = reporter.activityTimer(
        `Snapsen -- Creating audio sprites for '${node.fileAbsolutePath}'`
      )
      audioSpriteActivity.start()
      await createAudiospriteNodes({
        mdNode: node,
        audioVoices: audioVoices,
        createNode,
        createNodeId,
        createParentChildLink,
        cache,
        reporter,
      })
      audioSpriteActivity.end()
    }

    const startnote = songFrontmatter?.startnote
    if (startnote instanceof Array) {
      const notes = startnote.map((fmNote: FrontmatterNote) => {
        return noteShorthand2Note(
          fmNote.note,
          fmNote.clefSign,
          fmNote.clefLine,
          fmNote.clefOctaveChange
        )
      })

      reporter.verbose(
        `Snapsen -- Creating start note SVGs for node ${
          node?.fileAbsolutePath
        }: ${JSON.stringify(startnote, null, 4)}`
      )
      await Promise.all(
        notes.map((singleNote) =>
          createSingleNoteMxl({
            fileNode: node,
            createNode,
            createNodeId,
            createParentChildLink,
            cache,
            reporter,
            ...singleNote,
          })
        )
      )
    } else if (startnote != null) {
      // If 'startnote' frontmatter field is defined, but not an array, print
      // an error.
      reporter.error(
        `Snapsen -- Field 'startnote' in song frontmatter of '${node?.fileAbsolutePath}' should be an array, ignoring field`
      )
    }
  }
}

// ----- Create pages ---------------------------------------------------------

export const createPages: GatsbyNode["createPages"] = async ({
  graphql,
  actions,
  createNodeId,
  createContentDigest,
  reporter,
  cache,
  getNode,
}) => {
  const { createPage, createNode, createParentChildLink } = actions

  // Query song data
  const result: any = await graphql(`
    query {
      allSongs: allMarkdownRemark {
        nodes {
          id
          title
          contentFull
          slug
          book {
            slug
          }
          startnote {
            noteStep
            noteAlter
            noteOctave
          }
          frontmatter {
            page
            melody
            melodyCredit
            lyricsCredit
            startnote {
              note
              clefSign
              clefLine
              clefOctaveChange
            }
          }
        }
      }
    }
  `)
  if (result.errors) {
    throw result.errors
  }

  const allSongs: Song[] = result.data.allSongs.nodes

  const songTemplate = resolve("src/templates/song.tsx")

  const allNotes = result.data.allSongs.nodes.flatMap((song: Song) => {
    return song?.startnote ? song.startnote : []
  })
  const uniqueNotes = uniqWith(allNotes, isEqual)
  const outFiles = [
    path.join(__dirname, ".cache", "snapsenTones.webm"),
    path.join(__dirname, ".cache", "snapsenTones.mp3"),
  ]
  const siteNode: Node | undefined = getNode("Site")

  // Create sine tones when at least one start tone is defined
  if (uniqueNotes.length) {
    await createToneAudiospriteNodes({
      siteNode,
      createNode,
      createNodeId,
      createParentChildLink,
      cache,
      reporter,
      notes: uniqueNotes,
      toneDuration: 4,
      fadeDuration: 0.2,
      outFiles,
    })
  }

  // Create search index
  const searchActivity = reporter.activityTimer(
    `Snapsen -- creating search index`
  )
  searchActivity.start()
  await createSongSearch({
    allSongs,
    createNode,
    createNodeId,
    createContentDigest,
  })
  searchActivity.end()

  const songActivity = reporter.activityTimer(
    `Snapsen -- templating song pages`
  )
  songActivity.start()
  // Create a song details page for each song
  allSongs.forEach((song: Song, songIndex: number) => {
    createPage({
      path: `/${song.book.slug}-${song.slug}`,
      component: songTemplate,
      context: {
        songSlug: song.slug,
        index: songIndex,
        bookSlug: song.book.slug,
      },
    })

    songActivity.setStatus(`templated ${songIndex + 1}/${allSongs.length}`)
  })
  songActivity.end()
}
