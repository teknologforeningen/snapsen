const OSMDWrapper = require("./OSMDWrapper")
const { parentPort } = require("node:worker_threads")

const wrapper = new OSMDWrapper()

let promise = null

parentPort.on("message", async (message) => {
  if (message.type === "setSizes") {
    wrapper.setSizes(message.sizes)
  } else if (message.type === "setSingleNote") {
    wrapper.setSingleNote(message.singleNote)
  } else {
    // Wait for previous calls
    while (promise) await promise
    const { path, isMxl, sizes } = message
    promise = wrapper.generateSVGs(path, isMxl, sizes)
    try {
      const svgs = await promise
      parentPort.postMessage({ status: "success", path, svgs })
    } catch (e) {
      parentPort.postMessage({ status: "error", error: e.message })
    } finally {
      promise = null
    }
  }
})
