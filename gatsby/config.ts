import { readFileSync, accessSync, constants } from "fs" // for reading YAML files
import { load } from "js-yaml" // for reading YAML files
import { resolve } from "path"
import type {
  SnapsenConfig,
  Book,
  SnapsenTag,
  SnapsenTagLabelMap,
} from "../src/types/snapsenConfig"
import type { PluginRef } from "gatsby"
import { Solver, Color } from "../src/utils/cssFilterGenerator"
import chroma from "chroma-js"

function checkFileExists(filepath: string) {
  let flag = true
  try {
    accessSync(filepath, constants.F_OK)
  } catch (e) {
    flag = false
  }
  return flag
}

// Read YAML file from path specified in env variable CONFIG.
// If the env variable does not exist, use config.yml in current dir.
// TODO: Make file paths OS-independent, handle relative filepaths
const snapsenConfigPath = process.env.SNAPSEN_CONFIG
  ? `${process.env.SNAPSEN_CONFIG}`
  : checkFileExists("config.yml")
    ? resolve("config.yml")
    : resolve("gatsby/defaultConfig.yml")

console.log(`Snapsen -- Using '${snapsenConfigPath}' as config file.`)

const snapsenCustomConfig = load(
  readFileSync(snapsenConfigPath, "utf8")
) as SnapsenConfig

const snapsenDefaultConfig = load(
  readFileSync("./gatsby/defaultConfig.yml", "utf8")
) as SnapsenConfig

export const config: SnapsenConfig = {
  ...snapsenDefaultConfig,
  ...snapsenCustomConfig,
}

const {
  title,
  description,
  author,
  siteUrl,
  siteName,
  keywords,
  appIconPath,
  iconPath,
  books,
  colorsLight,
  colorsDark,
  breakpoints,
  tags,
  defaultType,
  songListLoadThreshold,
  songListSearchLimit,
  useCategoryIcons,
  sheetOnly,
  languages,
  defaultLanguage,
  redirectLanguage,
} = config

const bookToPlugin = (book: Book): PluginRef => {
  switch (book.source) {
    case "git": {
      const { name, remote, branch, patterns } = book
      return {
        resolve: "gatsby-source-git",
        options: {
          name,
          remote,
          branch,
          patterns,
        },
      }
    }
    case "filesystem": {
      const { name, path, ignore } = book
      return {
        resolve: "gatsby-source-filesystem",
        options: {
          name,
          path,
          ignore,
        },
      }
    }
  }
}

const typeColorToFilter = (
  types: SnapsenTagLabelMap[],
  defaultColorFilter: string
) => {
  return types.map((typeTag) => {
    const color = typeTag?.color ? typeTag.color : colorsLight.fg

    const darkColor = typeTag?.darkColor
      ? typeTag.darkColor
      : typeTag?.color
        ? chroma(typeTag.color).darken(1).hex()
        : colorsDark.fg

    const colorFilter = typeTag?.color
      ? hexColorToSvgFilter(typeTag.color)
      : defaultColorFilter
    const darkColorFilter = typeTag?.darkColor
      ? hexColorToSvgFilter(typeTag.darkColor)
      : typeTag?.color
        ? colorFilter
        : defaultColorFilter

    return {
      ...typeTag,
      color,
      darkColor,
      colorFilter,
      darkColorFilter,
    }
  })
}

function hexColorToSvgFilter(hexColor: string) {
  // Calculates the CSS filter string to get as close as possible to the given
  // color. Loops until loss is less than 0.5, or max times.
  const svgColor = new Color(chroma(hexColor).rgb())
  const solver = new Solver(svgColor)
  let result = solver.solve()
  const max = 20

  for (let i = 0; i < max && result.loss > 0.01; i++) {
    result = solver.solve()
  }
  const svgColorFilter: string = result?.filter
    ? result?.filter?.replace(/^(filter: )/, "")
    : ""
  return svgColorFilter
}

export const bookPlugins = books.map((book) => bookToPlugin(book))

const colorsLightFilters = Object.fromEntries(
  Object.keys(colorsLight)
    .map((color: string) => {
      if (color === "fg") {
        return [color, hexColorToSvgFilter(colorsLight[color])]
      }
    })
    .filter((item) => item)
)

const colorsDarkFilters = Object.fromEntries(
  Object.keys(colorsDark)
    .map((color: string) => {
      if (color === "fg") {
        return [color, hexColorToSvgFilter(colorsDark[color])]
      }
    })
    .filter((item) => item)
)

const newTags = tags.map((tag: SnapsenTag) => {
  if (tag?.value === "type") {
    return {
      ...tag,
      labelMap: typeColorToFilter(tag?.labelMap, colorsLightFilters.fg),
    }
  } else {
    return tag
  }
})

export const siteMetadata = {
  title,
  description,
  author,
  siteUrl,
  siteName,
  keywords,
  appIconPath,
  iconPath,
  colorsLight,
  colorsLightFilters,
  colorsDark,
  colorsDarkFilters,
  breakpoints,
  tags: newTags,
  defaultType: typeColorToFilter([defaultType], colorsLightFilters.fg)[0],
  songListLoadThreshold,
  songListSearchLimit,
  useCategoryIcons,
  sheetOnly,
  languages,
  defaultLanguage,
  redirectLanguage,
}
