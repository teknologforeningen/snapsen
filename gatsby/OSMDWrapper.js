const { JSDOM } = require("jsdom")
const { readFileSync } = require("fs")

// Options used for OSMD
const OSMDOptions = {
  drawingParameters: "compacttight",
  autoResize: false,
  drawCredits: false,
  drawComposer: false,
  drawLyricist: false,
  drawTitle: false,
  drawSubtitle: false,
  pageFormat: "Endless",
  backend: "svg",
  // OSMD does not yet support changing all colors of sheet
  // (clef/time signature/etc missing).
  // Github Issue: https://github.com/opensheetmusicdisplay/opensheetmusicdisplay/issues/507
  //colorStemsLikeNoteheads: true, // Color the stems of notes the same as their noteheads.
  //defaultColorNotehead: colors.fg , // Color of note heads.
  //defaultColorLabel: colors.fg, // Color of title, lyrics, etc.
  //defaultColorRest: colors.fg, // Color of rests.
  defaultFontFamily: "Times new Roman", // Default font.
  drawTimeSignatures: true, // Draw time signatures.
  measureNumberInterval: 5, // Draw measure number every n measures.
  //pageBackgroundColor: colors.bg, // Background color (RRGGBBOO).
}

// ---- hacks to fake Browser elements OSMD and Vexflow need, like window, document, and a canvas HTMLElement ----
const dom = new JSDOM("<!DOCTYPE html></html>")
const window = dom.window
window.console = console // probably does nothing

global.window = window
global.document = window.document
global.HTMLElement = window.HTMLElement
global.HTMLAnchorElement = window.HTMLAnchorElement
global.XMLHttpRequest = window.XMLHttpRequest
global.DOMParser = window.DOMParser
global.Node = window.Node
global.Blob = Blob

// hack: set height and width reliably
const getHeight = {
  get: function () {
    return this.height
  },
}
const getWidth = {
  get: function () {
    return this.width
  },
}
Object.defineProperties(window.HTMLElement.prototype, {
  offsetLeft: {
    get: function () {
      return parseFloat(window.getComputedStyle(this).marginTop) || 0
    },
  },
  offsetTop: {
    get: function () {
      return parseFloat(window.getComputedStyle(this).marginTop) || 0
    },
  },
  offsetHeight: getHeight,
  offsetWidth: getWidth,
  clientHeight: getHeight,
  clientWidth: getWidth,
  scrollHeight: getHeight,
  scrollWidth: getWidth,
})

// window needs to be available before we can require OSMD
// Must use require, static imports are hoisted to top of file and dynamic imports appear to be broken
const { OpenSheetMusicDisplay, MXLHelper } = require("opensheetmusicdisplay")

class OSMDWrapper {
  div
  instance
  sizes

  constructor() {
    const height = 32767
    this.singleNote = false

    this.div = window.document.createElement("div")
    this.div.id = "browserlessDiv"
    window.document.body.appendChild(this.div)

    // Width set later
    this.div.height = height
    this.div.setAttribute("height", height.toString())

    this.instance = new OpenSheetMusicDisplay(this.div, OSMDOptions)
    this.instance.setLogLevel("info") // doesn't seem to work, log.debug still logs
  }

  setSingleNote(singleNote) {
    this.singleNote = singleNote
    if (singleNote) {
      this.instance.setOptions({ drawTimeSignatures: false })
    }
  }

  async generateSVGs(path, isMxl, genSizes) {
    const content = readFileSync(path) // Using loadNodeContent corrupts mxl

    let musicXML
    if (isMxl) {
      musicXML = await MXLHelper.MXLtoXMLstring(content)
    } else {
      musicXML = content.toString()
    }
    await this.instance.load(musicXML) // if using load.then() without await, memory will not be freed up between renders
    const svgs = genSizes.map((s) => this._generateSVG(s))

    return svgs
  }

  _setWidth(width) {
    this.div.width = width
    this.div.setAttribute("width", width.toString())
    this.div.setAttribute("offsetWidth", width.toString())
  }

  _generateSVG(size) {
    this._setWidth(size.width)
    this.instance.zoom = size.zoom
    this.instance.render() // there were reports that await could help here, but render isn't a synchronous function, and it seems to work. see #932

    const svgElement = this.div.querySelector("#osmdSvgPage1")
    // The important xmlns attribute is not serialized unless we set it here
    svgElement.setAttribute("xmlns", "http://www.w3.org/2000/svg")
    const svg = svgElement.outerHTML //.replace(/black/gi, colors.fg).replace(/\#000000/gi, colors.fg));

    if (this.div.querySelector("#osmdSvgPage2")) {
      throw new Error("OSMDWrapper only supports single page rendering!")
    }

    return svg
  }
}

module.exports = OSMDWrapper
