// OSMDWrapper and dependencies. Used to create sheet music SVGs.
import { generateSVGs, SheetSize, setSingleNote } from "./OSMDManager"
import { Buffer } from "buffer"
import {
  createFileNodeFromBuffer,
  CreateFileNodeFromBufferArgs,
  FileSystemNode,
} from "gatsby-source-filesystem"
import gatsby from "gatsby"

interface CreateSheetNodeArgs {
  fileNode: FileSystemNode
  createNode: CreateFileNodeFromBufferArgs["createNode"]
  createNodeId: CreateFileNodeFromBufferArgs["createNodeId"]
  createParentChildLink: gatsby.Actions["createParentChildLink"]
  cache: CreateFileNodeFromBufferArgs["cache"]
  singleNote: boolean
}

const zip = <T>(a: T[], b: T[]): [T, T][] => a.map((v, i) => [v, b[i]])

export const createSheetNodes: any = async function ({
  fileNode,
  createNode,
  createNodeId,
  createParentChildLink,
  cache,
  singleNote,
}: CreateSheetNodeArgs) {
  // lg = 75em - margins
  // md = 64em - margins
  // sm = 48em - margins

  //TODO: Generate only one SVG for single notes. Code below gives correct
  //results, but throws build errors.
  const sizes: string[] = !!singleNote ? ["note"] : ["small", "medium", "large"]
  const sheetSizes: SheetSize[] = !!singleNote
    ? [{ size: "note", width: 110, zoom: 1 }]
    : [
        { size: "small", width: 536, zoom: 0.8 },
        { size: "medium", width: 768, zoom: 0.8 },
        { size: "large", width: 950, zoom: 0.7 },
      ]

  const getCached = async (size: string): Promise<string> => {
    const cacheKey = `${fileNode.internal.contentDigest}-sheet-${size}`
    return await cache.get(cacheKey)
  }

  const setCached = async (size: string, svg: string) => {
    const cacheKey = `${fileNode.internal.contentDigest}-sheet-${size}`
    // missing return?
    await cache.set(cacheKey, svg)
  }

  const createSVGFileNode = async (size: string, svg: string) => {
    if (typeof svg === "undefined") {
      console.log("Error: typeof svg is undefined!")
      console.log(`File: ${fileNode.relativePath}`)
      console.log(`Size: ${size}`)
    }
    const buffer = Buffer.from(svg)
    return await createFileNodeFromBuffer({
      buffer,
      name: `${fileNode.name}__${size}`,
      ext: ".svg",
      parentNodeId: fileNode.id,
      createNode,
      createNodeId,
      cache,
    })
  }

  //store: undefined, // Error in typings, not actually used
  let svgs = await Promise.all(sizes.map((s) => getCached(s)))

  // Regenerate if one is missing
  if (svgs.some((s) => !s)) {
    const isMxl =
      fileNode.internal.mediaType === "application/vnd.recordare.musicxml"

    // Change engraving options if we are rendering only one note on a single
    // staff
    setSingleNote(singleNote)

    svgs = await generateSVGs(fileNode.absolutePath, isMxl, sheetSizes)

    await Promise.all(zip(sizes, svgs).map((pair) => setCached(...pair)))
  }

  const nodes = await Promise.all(
    zip(sizes, svgs).map((pair) => createSVGFileNode(...pair))
  )

  nodes.forEach((n) => createParentChildLink({ parent: fileNode, child: n }))
}
