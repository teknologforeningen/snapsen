import { Document } from "flexsearch"
//import { Song, SongTag } from "../src/types/song"
import {
  DocumentOptions,
  StoreOption,
  IndexOptionsForDocumentSearch,
} from "flexsearch"

// TODO: Fix type
const indexFields: any = [
  {
    field: "page",
    tokenize: "strict",
    resolution: 2,
    minlength: 1,
  },
  {
    field: "title",
    tokenize: "full",
    resolution: 2,
    minlength: 1,
  },
  {
    field: "melody",
    tokenize: "full",
    resolution: 2,
    minlength: 1,
  },
  {
    field: "melodyCredit",
    tokenize: "full",
    resolution: 2,
    minlength: 1,
  },
  {
    field: "lyricsCredit",
    tokenize: "full",
    resolution: 2,
    minlength: 1,
  },
  {
    field: "contentFull",
    tokenize: "full",
    resolution: 2,
    minlength: 1,
  },
]
const documentOpts: DocumentOptions<StoreOption> = {
  id: "id",
  index: indexFields,
}
const indexOpts: IndexOptionsForDocumentSearch<string> = {
  tokenize: "full",
  charset: "latin:simple",
  resolution: 5,
  //minlength: 1,
  optimize: true,
  cache: true,
  context: false,
  document: documentOpts,
}

function sleep(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export const createSongSearch = async function ({
  allSongs, // all song nodes
  createNode,
  createNodeId,
  createContentDigest,
}) {
  const index = new Document(indexOpts)

  allSongs.forEach((song: any) => {
    const songData = {
      id: song.id,
      title: song.title,
      page: song.frontmatter.page ? song.frontmatter.page.join(" ") : undefined,
      contentFull: song.contentFull,
      melody: song.frontmatter.melody,
      melodyCredit: song.frontmatter.melodyCredit,
      lyricsCredit: song.frontmatter.lyricsCredit,
    }

    index.add(song.id, songData as any)
  })

  const exportedIndex = {}
  index.export(function (key, data) {
    exportedIndex[key] = data
  })

  // Horrible hack to resolve a problem where the compiled code of flexsearch
  // uses setTimeout with zero delay instead of promises in the export function.
  // The only way to know when the export is done is to check for the 'store'
  // key on the export object, as it is the last key to be exported.
  while (!("store" in exportedIndex)) {
    await sleep(100)
  }

  const indexString = JSON.stringify(exportedIndex)
  const optsString = JSON.stringify(indexOpts)
  const indexNodeId = createNodeId("SongSearch")
  const node = {
    id: indexNodeId,
    index: indexString,
    options: optsString,
    internal: {
      type: "SongSearch",
      contentDigest: createContentDigest(indexString),
    },
  }
  createNode(node)

  const testIndex = new Document(indexOpts)
  const parsed = JSON.parse(indexString)
  for (const key in parsed) {
    testIndex.import(key, parsed[key])
  }
}
