---
# Snapsen default configuration
#
# Override one or several options by creating a custom config in ../config.yml
# or by giving an absolute file path to a config file in the environment
# variable SNAPSEN_CONFIG when running/building.

# Metadata used in meta tags. Be sure to set siteUrl to the actual URL used for
# your site, otherwise all links in meta tags will be wrong.

title: Snapsen
description: Snapsen digital songbook
author: Teknologföreningen
siteUrl: https://snapsen-song-book.netlify.app
siteName: Snapsen
keywords:
  - songbook
  - snapsen

# Different language versions to use. The default language will be available on
# `/`, while other languages will have all paths prefixed with the language
# code.

defaultLanguage: en
languages:
  - en
  - sv
  - fi

# Sets if users should automatically be redirected to the correct language
# version depending on their browser settings.
redirectLanguage: false

# Icon used for the PWA metadata
appIconPath: "src/images/snapsen-icon.png"

# Icon used in site header.
# NOTE: The file must have the extension ".inline.svg", otherwise it will not
# work.
iconPath: "src/images/glass-icon.inline.svg"

# Book sources
books:
  - source: "filesystem"
    name: local-test-book
    path:
      "./books/local_test_book"
      #  - source: "git"
      #    name: snapsen-example-songbook
      #    remote: "https://gitlab.com/teknologforeningen/snapsen-example-songbook.git"
      #    patterns:
      #      - "*.md"
      #      - "**/*.md"
      #      - "*.yml"
      #      - "**/*.mp3"
      #      - "**/*.mxl"
      #      - "!README.md"

# Color configuration for light and dark mode separately. Shades of colors are
# automatically generated. The parameter shadeFactor changes how strongly the
# color lightens/darkens.
#
# For example, in light mode:
#
# The CSS color `--neutral0` is the exact same color as colorsLight.neutral.
# Darker shades from `--neutral5` to `--neutral90` are automatically generated.
# By increasing `shadeFactor`, the shades will get darker quicker.
#
# The same logic applies in dark mode, but the shades will get lighter instead
# of darker.

colorsLight:
  fg: "#001627" # dark blue
  bg: "#ffffe8" # cream white
  neutral: "#ffffe8" # cream white
  primary: "#f1e792" # gold
  alert: "#b20738" # teknologröd
  accent1: "#9ad7d7" # blue
  accent2: "#d79ad1" # pink
  shadeFactor: 5

colorsDark:
  fg: "#ffffe8" # cream white
  bg: "#001627" # dark blue
  neutral: "#001627" # dark blue
  primary: "#7b7554" # gold fusion
  alert: "#b20738" # teknologröd
  accent1: "#9ad7d7" # blue
  accent2: "#d79ad1" # pink
  shadeFactor: 10

# Page breakpoints in px.
#
# TODO: Also use for widths of the small, medium, and large generated sheet
# music.

breakpoints:
  xs: 320
  sm: 536
  md: 768
  lg: 950

# Song tags which can be used to filter searches. The parameters are the
# following:
#
#   value: The tag value that needs to be specified in song frontmatter.
#   label: Label of the tag, visible in the search field as a header above the
#     values.
#   color: Color of the tag.
#   labelMap: Set the values and labels of possible tag values.
#
# The labelMap is a list of dicts with the following parameters:
#
#   value: The tag value that needs to be specified in song frontmatter.
#   label: Tag label shown on song page. The tag will have the color specified
#     in the parent tag element.
#   shortLabel: A shorter version of the label, displayed on song cards in
#     search results.
#
#   Two of the parameters are only available on the `tyṕe` tag. These are:
#
#   icon: Category icon, shown on song cards in search results. Available icons
#     can be found in `../src/images/cat_icons`.
#   color: Color of the icon.

tags:
  - label: "Book"
    value: "book"
    color: "#b20738"
  - label: "Category"
    value: "type"
    color: "#9ad7d7"
    labelMap:
      - value: ordered
        label: "Ordered songs"
        shortLabel: "Ord."
        color: "#9ad7d7" # blue
        icon: spirit
      - value: spirits
        label: "Spirits"
        shortLabel: "Sp."
        color: "#6e7dab" # glaucuous
        icon: shot
      - value: other
        label: "Other"
        shortLabel: "Oth."
        color: "#7d8ca3" # shadow blue
        icon: mug
      - value: punsch
        label: "Punsch"
        shortLabel: "Pu."
        color: "#f1e792" # gold
        icon: punsch
      - value: wine
        label: "Wine"
        shortLabel: "Wi."
        color: "#b20738" # teknologröd
        icon: wine
      - value: seasonal
        label: "Seasonal"
        shortLabel: "Seas."
        color: "#3e78b2" # bright navy blue
        icon: seasonal
      - value: ceremonial
        label: "Ceremonial"
        shortLabel: "Cer."
        color: "#fa824c" # mango tango
        icon: champagne

  - label: Order
    value: songOrder
    color: "#6e7dab" # glaucuous
    labelMap:
      - value: "1"
        label: "First"
        shortLabel: "1st"
      - value: "2"
        label: "Second"
        shortLabel: "2nd"
      - value: "3"
        label: "Third"
        shortLabel: "3rd"
      - value: "4"
        label: "Fourth"
        shortLabel: "4th"
      - value: "5"
        label: "Fifth"
        shortLabel: "5th"
      - value: "6"
        label: "Sixth"
        shortLabel: "6th"
      - value: "7"
        label: "Seventh"
        shortLabel: "7th"
      - value: "-1"
        label: "Final"
        shortLabel: "Fin."

# Default song type in case category icons are enabled, but the song has no
# defined type.

defaultType:
  value: unknown
  label: "Unknown songs"
  shortLabel: "Unk."
  color: "#f1e792" # gold
  icon: bottle

# Sets if category icons should be displayed in the search results.
useCategoryIcons: true

# If set to `true`, song lyrics will not be displayed on song pages. Instead
# the sheet music will be displayed immediately, without the need to open a
# dropdown.
#
# NOTE: It is still advised to put the lyrics of the song in the markdown file
# even if you use `sheetOnly: true`. The lyrics are still used for:
#   - searching
#   - excerpt displayed on song cards in search results
sheetOnly: false

# Option used by react-virtuoso. When displaying search results, react-virtuoso
# will load (amount of visible cards that can fit in the layout +
# songListLoadThreshold) cards. If there are less than songListLoadThreshold
# loaded (but not visible) cards when the user starts scrolling, the next set
# of cards will be loaded.
songListLoadThreshold: 20

# Option used by flexsearch. Sets how many search results will be returned "per
# loop" when searching using flexsearch.
songListSearchLimit: 25
