import slugify from "underscore.string/slugify"
import { join, dirname } from "path"
import { config } from "./config"
import { getNoteNodeName, noteShorthand2Note } from "../src/utils/noteUtils"
import { Note, FrontmatterNote } from "../src/types/song"
import {
  getAudiospriteFileKey,
  getAudiospriteMetaFileKey,
} from "./createAudiospriteNodes"

// Markdown node resolvers

const htmlASTToContentText = (node) => {
  if (
    node.tagName === "h1" ||
    node.tagName === "h2" ||
    node.tagName === "h3" ||
    node.tagName === "h4" ||
    node.tagName === "h5" ||
    node.tagName === "h6" ||
    node.tagName === "blockquote"
  ) {
    return ""
  } else if (
    (node.type === "text" || node.type === "inlineCode") &&
    node.value
  ) {
    return node.value
  } else if (node.children) {
    let str = ""
    //for (child of node.children)
    node.children.forEach((child) => {
      str += htmlASTToContentText(child)
    })
    return str
  }
}

const mdFieldResolver =
  (fieldName: string) =>
  async (source, args, context, { schema }) => {
    const type = schema.getType("MarkdownRemark")
    const resolver = type.getFields()[fieldName].resolve
    return await resolver(source, args, context, { fieldName })
  }

const mdResolveHeadings = mdFieldResolver("headings")

export const mdResolveTitle = async (source, args, context, info) => {
  const headings = await mdResolveHeadings(source, {}, context, info)
  if (headings && headings.length) {
    return headings[0].value
  } else {
    const fileNode = context.nodeModel.getNodeById({
      id: source.parent,
      type: "File",
    })
    return fileNode.name
  }
}

export const mdResolveSongSlug = async (source, args, context, info) =>
  slugify(
    source.frontmatter.slug || (await mdResolveTitle(source, {}, context, info))
  )

const resolveHtmlAST = mdFieldResolver("htmlAst")

export const resolveContentFull = async (source, args, context, info) => {
  const ast = await resolveHtmlAST(source, {}, context, info)
  return htmlASTToContentText(ast)
}

const tagMetas = Object.fromEntries(config.tags.map((tag) => [tag.value, tag]))

/**
 * Create a single song tag
 * @param tag {string} Name of the tag, e.g. "book"
 * @param value {string} Value of the tag, e.g. "My Song Book"
 * @param [labelExtension] {string | undefined} Optional extra string to put after the tag label
 */
const songTag = (
  tag: string,
  value: string,
  labelExtension: string | undefined = undefined
) => {
  const stringValue = value ? value.toString() : null
  const meta = tagMetas[tag]
  const label = meta.labelMap
    ? meta.labelMap.find((element) => element.value === stringValue)?.label
    : stringValue
  const shortLabel = meta.labelMap
    ? meta.labelMap.find((element) => element.value === stringValue)?.shortLabel
    : abbreviateString(stringValue.toString())
  const mdash = "–"
  return {
    tag,
    tagLabel: meta.label,
    value: label || shortLabel ? label : null,
    label: labelExtension ? `${label} ${mdash} ${labelExtension}` : label,
    shortLabel: labelExtension
      ? `${shortLabel} ${mdash} ${labelExtension}`
      : shortLabel,
    color: meta.color,
  }
}

// TODO: Improve regex
const abbreviateString = (longString) => {
  return longString.match(/[A-Z0-9]/g)
    ? longString.match(/[A-Z0-9]/g).join("")
    : longString
}

export const resolveTags = async (source, args, context, info) => {
  const book = await resolveBook(source, {}, context, info)

  // Add 'book' to frontmatter fields
  const entries = Object.entries(source.frontmatter || {})
  entries.push(["book", book.title])

  // Get song page number from frontmatter
  const pageField = source.frontmatter.page
  const page = Array.isArray(pageField) ? pageField[0] : pageField

  // Get all tags defined in 'tagMetas' from the frontmatter fields. Fields
  // that have lists as values create individual tags, flatMap is used to
  // flatten the nested list caused by this.
  return entries
    .filter(([tag]) => tag in tagMetas)
    .flatMap(([tag, value]: [string, string]) => {
      // Add book tag with song page number appended to book title.
      if (tag === "book" && page) {
        // TODO: Internationalisation
        return songTag(tag, value, `s. ${page}`)
      }

      // If frontmatter tag is a list, add each value as a separate tag.
      else if (Array.isArray(value)) {
        return value.map((v) => songTag(tag, v))
      }

      // Otherwise just add a single tag.
      else {
        return songTag(tag, value)
      }
    })
    .filter((tag) => tag.value !== null) // Remove null values
}

export const resolveBook = async (source, args, context, info) => {
  const fileNode = context.nodeModel.getNodeById({
    id: source.parent,
    type: "File",
  })
  return await context.nodeModel.findOne({
    type: "YamlBook",
    query: {
      filter: { sourceInstanceName: { eq: fileNode.sourceInstanceName } },
    },
  })
}

export const sheetResolver =
  (size: string) => async (source, args, context, info) => {
    if (!source.frontmatter.sheet) {
      return null
    }
    const musicXmlPath = join(
      dirname(source.fileAbsolutePath),
      source.frontmatter.sheet
    )
    const musicXmlFileNode = await context.nodeModel.findOne({
      type: "File",
      query: {
        filter: { absolutePath: { eq: musicXmlPath } },
      },
    })
    if (!musicXmlFileNode) {
      return null
    }
    return await context.nodeModel.findOne({
      type: "File",
      query: {
        filter: {
          parent: { id: { eq: musicXmlFileNode.id } },
          name: { eq: `${musicXmlFileNode.name}__${size}` },
        },
      },
    })
  }

export interface FMAudio {
  name: string
  file?: string
  voices?: FMAudioVoice[]
}
export interface FMAudioVoice {
  name: string
  file: string
}
export const audioResolver = async (source, args, context, info) => {
  if (!source.frontmatter.audio) {
    return null
  }
  const mdNodeId = source.id

  return await Promise.all(
    source.frontmatter.audio.map(async (audio: FMAudio) => {
      const audioName = audio.name || source.id

      const singleFilePath = join(
        dirname(source.fileAbsolutePath || ""),
        audio?.file || ""
      )
      const voices = audio?.voices
      if (voices) {
        const audiospriteFileNode = await context.nodeModel.findOne({
          type: "File",
          query: {
            filter: {
              parent: { id: { eq: mdNodeId } },
              name: {
                eq: getAudiospriteFileKey({
                  parentNodeId: mdNodeId,
                  audio: audio,
                }),
              },
            },
          },
        })
        const audiospriteMetaNode = await context.nodeModel.findOne({
          type: "File",
          query: {
            filter: {
              parent: { id: { eq: mdNodeId } },
              name: {
                eq: getAudiospriteMetaFileKey({
                  parentNodeId: mdNodeId,
                  audio: audio,
                }),
              },
            },
          },
        })
        const audiospriteMeta = JSON.parse(
          audiospriteMetaNode?.internal?.content
        )?.sprite
        if (audiospriteFileNode && audiospriteMeta) {
          return {
            name: audioName,
            file: audiospriteFileNode,
            voices: audiospriteMeta,
          }
        } else {
          return null
        }
      } else if (singleFilePath) {
        return {
          name: audioName,
          file: await context.nodeModel.findOne({
            type: "File",
            query: {
              filter: {
                absolutePath: { eq: singleFilePath },
              },
            },
          }),
        }
      }
    })
  )
}
// YamlBook node resolvers

export const resolveSourceInstanceName = (source, args, context, info) => {
  const fileNode = context.nodeModel.getNodeById({
    id: source.parent,
    type: "File",
  })
  return fileNode.sourceInstanceName
}

export const resolveBookSlug = (source, args, context, info) =>
  slugify(source.slug || resolveSourceInstanceName(source, {}, context, info))

/** Resolve start notes (written in note shorthand) from
 * node.frontmatter.startnote into full Note objects. Also adds a field
 * "svgFile" which points to the File node of the generated SVG of the start
 * note.
 */
export const startNoteResolver = async (source, args, context, info) => {
  if (!source?.frontmatter?.startnote) {
    return null
  }

  // const mdNodeId = source.id
  const startNotes = source.frontmatter.startnote.map(
    (fmNote: FrontmatterNote) => {
      return noteShorthand2Note(
        fmNote.note,
        fmNote.clefSign,
        fmNote.clefLine,
        fmNote.clefOctaveChange
      )
    }
  )

  return await Promise.all(
    startNotes.map(async (startNote: Note) => {
      // Attempt to find a child node to the MarkdownRemark node with the name
      // `noteNodeName`, i.e. the generated MXL file.
      const noteNodeName = getNoteNodeName(startNote)
      const noteMxlFileNode = await context.nodeModel.findOne({
        type: "File",
        query: {
          filter: {
            //parent: { id: { eq: mdNodeId } },
            name: { eq: noteNodeName },
            extension: { eq: "mxl" },
          },
        },
      })

      // Attempt to find the SVG File node for this start note, which should be
      // a child to the MXL node.
      // NOTE: Could be extended to handle single note SVGs of different styles/sizes.
      const noteSvgFileNode = await context.nodeModel.findOne({
        type: "File",
        query: {
          filter: {
            parent: { id: { eq: noteMxlFileNode?.id } },
            name: {
              glob: `${noteNodeName}*`,
            },
            extension: { eq: "svg" },
          },
        },
      })

      return {
        name: noteNodeName,
        ...startNote,
        svgFile: noteSvgFileNode,
      }
    })
  )
}
