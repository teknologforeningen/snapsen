import { create } from "xmlbuilder2"
import { Buffer } from "buffer"
import {
  createFileNodeFromBuffer,
  CreateFileNodeFromBufferArgs,
  FileSystemNode,
} from "gatsby-source-filesystem"
import { Reporter, Actions } from "gatsby"
import Zip from "adm-zip"
import { getNoteNodeName } from "../src/utils/noteUtils"

interface CreateSingleNoteMxlArgs {
  fileNode?: FileSystemNode
  createParentChildLink: Actions["createParentChildLink"]
  createNode: CreateFileNodeFromBufferArgs["createNode"]
  createNodeId: CreateFileNodeFromBufferArgs["createNodeId"]
  cache: CreateFileNodeFromBufferArgs["cache"]
  reporter: Reporter
  noteStep: string
  noteOctave: number
  noteAlter: number
  noteLyric?: string
  clefSign: string
  clefLine: number
  clefOctaveChange: number
}

export const createSingleNoteMxl: any = async function ({
  fileNode,
  createNode,
  createNodeId,
  createParentChildLink,
  cache,
  reporter,
  noteStep,
  noteOctave,
  noteAlter,
  noteLyric,
  clefSign,
  clefLine,
  clefOctaveChange,
}: CreateSingleNoteMxlArgs) {
  let singleMxlNote = {
    "score-partwise": {
      "@version": "4.0",
      work: { "work-title": `${noteStep.toUpperCase()}` },
      identification: {
        encoding: {
          software: "Snapsen",
          "encoding-date": `${new Date().toJSON().slice(0, 10)}`,
          supports: [
            { "@element": "accidental", "@type": "yes" },
            { "@element": "beam", "@type": "yes" },
            {
              "@element": "print",
              "@attribute": "new-page",
              "@type": "no",
            },
            {
              "@element": "print",
              "@attribute": "new-system",
              "@type": "no",
            },
            { "@element": "stem", "@type": "yes" },
          ],
        },
      },
      "part-list": {
        "score-part": {
          "@id": "P1",
          "part-name": "Note",
          "part-abbreviation": "N.",
          "score-instrument": { "@id": "P1-I1", "instrument-name": "Note" },
        },
      },
      part: {
        "@id": "P1",
        measure: {
          "@number": "1",
          attributes: {
            divisions: "1",
            key: { fifths: "0" },
            clef: {
              sign: clefSign,
              line: clefLine,
              "clef-octave-change": clefOctaveChange,
            },
          },
          note: {
            pitch: {
              step: noteStep,
              alter: noteAlter,
              octave: noteOctave,
            },
            lyric: !!noteLyric
              ? {
                  "@number": "1",
                  syllabic: "single",
                  text: noteLyric,
                }
              : {},
            duration: "4",
            voice: "1",
            type: "whole",
          },
        },
      },
    },
  }

  const doc = create(singleMxlNote)
  const xml = doc.end()
  const nodeName = getNoteNodeName({
    noteStep: noteStep,
    noteAlter: noteAlter,
    noteOctave: noteOctave,
    clefSign: clefSign,
    clefLine: clefLine,
    clefOctaveChange: clefOctaveChange,
  })

  let zip = new Zip()
  zip.addFile("score.xml", Buffer.from(xml, "utf8"), "Single note musicxml")
  zip.addLocalFolder("./gatsby/singleNoteMxlMeta")
  let zipBuffer: Buffer | null = zip.toBuffer()
  const mxlNode: FileSystemNode = await createFileNodeFromBuffer({
    buffer: zipBuffer,
    name: nodeName,
    parentNodeId: fileNode?.id,
    ext: ".mxl",
    createNode,
    createNodeId,
    cache,
  })
  zipBuffer = null

  reporter.verbose(
    `Snapsen -- Created single note MXL ${nodeName} with ID ${mxlNode.id}`
  )
  fileNode && createParentChildLink({ parent: fileNode, child: mxlNode })
}
