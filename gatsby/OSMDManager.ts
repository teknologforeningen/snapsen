const { Worker } = require("node:worker_threads")

interface OSMDReturn {
  status: "success"
  path: string
  svgs: string[]
}

interface OSMDError {
  status: "error"
  path: string
  error: string
}

export interface SheetSize {
  size: string
  width: number
  zoom: number
}

interface PromiseData {
  resolve: (ret: string[]) => void | undefined
  reject: (err: any) => void | undefined
  promise: Promise<string[]> | undefined
}

const osmdWorker = new Worker("./gatsby/OSMDWorker.js", {
  stdout: true,
  stderr: true,
})
osmdWorker.unref()

const promiseData = new Map<string, PromiseData>()

osmdWorker.on("message", (message: OSMDReturn | OSMDError) => {
  const { resolve, reject } = promiseData.get(message.path)
  promiseData.delete(message.path)
  if (message.status === "error") {
    reject(new Error(message.error))
  } else {
    resolve(message.svgs)
  }
})

// Sets OSMD engraving rules appropriate for drawing a single note. Used for
// the tone player.
export const setSingleNote = (singleNote: boolean) => {
  osmdWorker.postMessage({ type: "setSingleNote", singleNote })
}

export const generateSVGs = (
  path: string,
  isMxl: boolean,
  sizes: SheetSize[]
): Promise<string[]> => {
  if (promiseData.has(path)) {
    return promiseData.get(path).promise
  } else {
    let resolve, reject
    const promise = new Promise<string[]>((res, rej) => {
      resolve = res
      reject = rej
    })
    promiseData.set(path, { resolve, reject, promise })
    osmdWorker.postMessage({ type: "generateSVGs", path, isMxl, sizes })
    return promise
  }
}
