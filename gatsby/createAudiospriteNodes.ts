import { Buffer } from "buffer"
import { readFileSync } from "fs"
import {
  createFileNodeFromBuffer,
  CreateFileNodeFromBufferArgs,
  FileSystemNode,
} from "gatsby-source-filesystem"
import gatsby from "gatsby"
import { join, dirname, extname, basename } from "path"
import audiosprite, { ExportType, LogLevel, Result } from "audiosprite"

import ffmpeg from "@ffmpeg-installer/ffmpeg"
import { spawn } from "node:child_process"
import { PathLike } from "fs"
import { Note } from "../src/types/song"
import { getNoteDistance, getNoteNodeName } from "../src/utils/noteUtils"
import { Reporter, Node } from "gatsby"

interface SongAudio {
  name: string
  file?: string
  voices?: SongAudioVoice[]
}
interface SongAudioVoice {
  name: string
  file: string
}

interface CreateAudiospriteNodesArgs {
  mdNode: FileSystemNode
  audioVoices: SongAudio[]
  createNode: CreateFileNodeFromBufferArgs["createNode"]
  createNodeId: CreateFileNodeFromBufferArgs["createNodeId"]
  createParentChildLink: gatsby.Actions["createParentChildLink"]
  cache: CreateFileNodeFromBufferArgs["cache"]
  reporter: Reporter
}

interface AudiospriteOutput {
  urls: string[]
  sprite: any
}

const createSpriteMeta = (obj: AudiospriteOutput, audio: SongAudio) => {
  interface Sprite {
    name: string
    file: string
    startTime: number
    duration: number
  }
  interface Meta {
    urls: string[]
    name: string | null
    sprite: Sprite[]
  }

  const sprite: Sprite[] = Object.keys(obj.sprite).map((spriteName: string) => {
    const timestamps = obj.sprite[spriteName]
    const voiceMeta = audio?.voices?.find((voice: SongAudioVoice) => {
      return basename(voice.file).replace(/\..*$/, "") === spriteName
    })
    return {
      ...voiceMeta,
      startTime: timestamps[0],
      duration: timestamps[1],
    }
  })

  const meta: Meta = {
    sprite,
    name: audio.name,
    urls: obj.urls,
  }
  return meta
}

interface CreateAudiospriteFileNodeArgs {
  name: string
  filePath: PathLike
  fileIndex: number
  nodeId: string
  createNode: CreateFileNodeFromBufferArgs["createNode"]
  createNodeId: CreateFileNodeFromBufferArgs["createNodeId"]
  cache: CreateFileNodeFromBufferArgs["cache"]
}

const createAudiospriteFileNode = async ({
  name,
  filePath,
  nodeId,
  createNode,
  createNodeId,
  cache,
}: CreateAudiospriteFileNodeArgs) => {
  let buffer: Buffer | null = Buffer.from(readFileSync(filePath))
  const node = await createFileNodeFromBuffer({
    buffer,
    name: name,
    ext: extname(filePath.toString()),
    parentNodeId: nodeId,
    createNode,
    createNodeId,
    cache,
  })
  buffer = null
  return node
}
/**
 * Creates a reproducible hash from a SongAudio object. Used by audioResolver
 * to match audio in frontmatter with generated audio sprites.
 *
 * NOTE: This could probably be achieved in some less stupid way.
 */
export const getFMAudioHash = (audio: SongAudio) => {
  const sortedVoices = audio.voices.sort(
    (a: SongAudioVoice, b: SongAudioVoice) => {
      const nameComp = a.name.localeCompare(b.name)
      // If the voice name is the same, compare file path
      if (nameComp == 0) {
        return a.file.localeCompare(b.file)
      }
      return nameComp
    }
  )

  const sortedVoicesStr = sortedVoices
    .map((audio: SongAudio) => `${audio.name}#${audio.file}`)
    .join("@")

  const audioString = audio?.name
    .toString()
    .concat("€", audio?.file)
    .concat("$", sortedVoicesStr)
  let hash = 0
  for (let i = 0, len = audioString.length; i < len; i++) {
    let chr = audioString.charCodeAt(i)
    hash = (hash << 5) - hash + chr
    hash |= 0
  }
  return hash
}

interface GetFileKeyArgs {
  parentNodeId: string
  audio: SongAudio
  fileType?: string
}

export const getFileKey = ({
  parentNodeId,
  audio,
  fileType,
}: GetFileKeyArgs) => {
  return `${parentNodeId}__${fileType}--${getFMAudioHash(audio)}`
}

export const getAudiospriteFileKey = ({
  parentNodeId,
  audio,
}: GetFileKeyArgs) => {
  return getFileKey({ parentNodeId, audio, fileType: "audiosprite" })
}

export const getAudiospriteMetaFileKey = ({
  parentNodeId,
  audio,
}: GetFileKeyArgs) => {
  return getFileKey({ parentNodeId, audio, fileType: "audiospritemeta" })
}

export const createAudiospriteNodes: any = async function ({
  mdNode,
  audioVoices,
  createNode,
  createNodeId,
  createParentChildLink,
  cache,
  reporter,
}: CreateAudiospriteNodesArgs) {
  const fileDir = dirname(mdNode.fileAbsolutePath as string)

  // Resolve full paths of files for voices
  const audioVoicesFullPaths = audioVoices.map((audio, audioIndex) => {
    return {
      parentId: mdNode.id,
      ...audio,
      audioIndex,
      audioFileKey: getAudiospriteFileKey({
        parentNodeId: mdNode.id,
        audio: audio,
      }),
      audioMetaFileKey: getAudiospriteMetaFileKey({
        parentNodeId: mdNode.id,
        audio: audio,
      }),
      voices: audio?.voices?.map((voice) => {
        return { ...voice, file: join(fileDir, voice.file) }
      }),
    }
  })

  reporter.verbose(
    `Snapsen -- Creating audio sprites for '${mdNode.fileAbsolutePath}': ${JSON.stringify(audioVoicesFullPaths, null, 4)}`
  )

  // TODO: Do not hardcode
  const audioDir = "./.cache/"
  const opts = {
    export: "webm,mp3",
    format: "howler" as ExportType,
    log: "info" as LogLevel,
    path: audioDir,
  }

  //TODO: Use cache
  //const getCached = async (name: string): Promise<string> => {
  //  const cacheKey = `${mdNode.internal.contentDigest}-audiosprite-${name}`
  //  return await cache.get(cacheKey)
  //}

  //const setCached = async (name: string, svg: string) => {
  //  const cacheKey = `${mdNode.internal.contentDigest}-audiosprite-${name}`
  //  await cache.set(cacheKey, svg)
  //}

  const createAudiospriteMetaFileNode = async (name: string, json: string) => {
    let buffer: Buffer | null = Buffer.from(json)
    reporter.verbose(
      `Snapsen -- Creating audio sprite metadata for '${mdNode.fileAbsolutePath}': ${json}`
    )
    const node = await createFileNodeFromBuffer({
      buffer,
      name: name,
      ext: ".json",
      parentNodeId: mdNode.id,
      createNode,
      createNodeId,
      cache,
    })
    buffer = null
    return node
  }

  audioVoicesFullPaths.forEach((audio) => {
    const outputPath = join(audioDir, audio.audioFileKey)
    const currentOpts = { ...opts, output: outputPath }
    reporter.verbose(
      `Snapsen -- Creating audio sprite for '${mdNode.fileAbsolutePath}' at ${outputPath}`
    )

    audiosprite(
      audio?.voices?.map((voice) => voice.file),
      currentOpts,
      function (err, obj: Result | AudiospriteOutput) {
        if (err) return console.error(err)
        const output = obj as AudiospriteOutput
        const spriteMeta = createSpriteMeta(output, audio)
        output.urls.forEach(async (fileUrl: string, fileIndex: number) => {
          const nodes = await Promise.all([
            createAudiospriteFileNode({
              name: audio.audioFileKey,
              filePath: fileUrl,
              fileIndex: fileIndex,
              nodeId: mdNode.id,
              createNode,
              createNodeId,
              cache,
            }),
            createAudiospriteMetaFileNode(
              audio.audioMetaFileKey,
              JSON.stringify(spriteMeta, null, 4)
            ),
          ])

          nodes.forEach((node) => {
            createParentChildLink({
              parent: mdNode,
              child: node,
            })
          })
        })
      }
    )
  })
}

function getPitch(basePitch: number, step: number) {
  return basePitch * Math.pow(2, step / 12)
}

export const generateToneSprite = (
  noteName: string[],
  duration: number,
  silenceDuration: number
) => {
  let sprite: { [key: string]: number[] } = {}
  noteName.forEach((nn, index) => {
    sprite[nn] = [
      index * (duration * 1000 + silenceDuration * 1000),
      duration * 1000,
    ]
  })
  return sprite
}

interface GenerateTonesProps {
  notes: Note[]
  outFiles: PathLike[]
  a4Pitch?: number
  harmonics?: number[]
  toneDuration: number
  fadeDuration: number
  silenceDuration: number
  reporter: Reporter
}

// Sine
//aevalsrc="sin(440*2*PI*t):s=8000"

// Sawtooth
//aevalsrc='0.25 * (mod(t*440, 2)-1.5)'

// Sine
//`sine=frequency=${note.pitch}:duration=${duration}`

/** Generate tones of different pitches using ffmpeg.
 *
 */
export const generateTones = async (props: GenerateTonesProps) => {
  const ffmpegPath = ffmpeg.path
  const { notes, outFiles, reporter } = props

  const repAct = reporter.activityTimer(
    `Snapsen -- Generate tone audio sprites`
  )
  repAct.start()
  const a4Pitch = typeof props?.a4Pitch !== "undefined" ? props.a4Pitch : 440

  const duration = props.toneDuration
  const fadeIn = props.fadeDuration
  const fadeOut = fadeIn
  const silenceDuration = props.silenceDuration

  let toneArgs: string[] = []
  let filterInputs: string[] = []
  const a4Note: Note = {
    noteStep: "A",
    noteOctave: 4,
    noteAlter: 0,
  }

  const silenceArgs = [
    "-f",
    "lavfi",
    "-t",
    silenceDuration.toString(),
    "-i",
    "anullsrc=channel_layout=stereo:sample_rate=44100",
  ]

  notes.forEach((note, noteIndex) => {
    const pitch = getPitch(a4Pitch, getNoteDistance(a4Note, note))

    // Approximate square wave by adding fundamental frequency and numHarmonics
    // odd harmonics
    const harmonics =
      typeof props?.harmonics !== "undefined" ? props.harmonics : [1]
    const aevalsrcSquare = harmonics
      .map((hmn) => {
        return `sin(${pitch}*2*PI*t*${hmn})/${hmn}`
      })
      .join("+")

    // Combine three filters to make the sound file for a single tone:
    // 1. aevalsrc for creating the tone
    // 2. afade with t=in for fading in
    // 2. afade with t=out for fading out
    const filters = [
      `aevalsrc=${aevalsrcSquare}:d=${duration}`,
      `afade=t=in:st=0:d=${fadeIn}`,
      `afade=t=out:st=${duration - fadeOut}:d=${fadeOut}`,
    ].join(",")

    toneArgs = [...toneArgs, "-f", "lavfi", "-i", filters, "-ac", "2"]

    // Create a list of inputs for the "concat" filter.
    //
    // Example: For an array of three notes, the complete "filterInputs" array
    // will be:
    //
    // [
    //    "[1:a][0:a]"
    //    "[2:a][0:a]"
    //    "[3:a][0:a]"
    // ]
    filterInputs = [...filterInputs, `[${noteIndex + 1}:a][0:a]`]
  })

  const volume = 1.0

  // Create arguments for "-filter_complex" for concatenating all tone inputs
  // (with silence inbetween) into a single audio file.
  //
  // Example: For an array of three notes, the complete "filterComplexArgs"
  // will be:
  //
  // [
  //    "-filter_complex",
  //    "[1:a][0:a][2:a][0:a][3:a][0:a]concat=n=6:v=0:a=1[b];[b]volume=1.0"
  // ]
  //
  // "[0:a]" is the first input given to ffmpeg (index 0), which is the audio
  // containing only silence. "[1:a]" is the first generated tone, "[2:a]" is
  // the second, etc. The "concat" filter concatenates these inputs in the
  // given order. The options given to the "concat" filter are:
  //
  //   n=6: Sets the number of segments to 6.
  //   v=0: Sets the number of output video streams to 0.
  //   a=1: Sets the number of output audio streams to 1.
  //
  // The "[b]" before the semicolon means that we take the output from "concat"
  // and name it "b". After the semicolon, we feed "b" as input to the "volume"
  // filter to change the volume if needed.

  const filterComplexArgs = [
    "-filter_complex",
    `${filterInputs.join("")}concat=n=${
      notes.length * 2
    }:v=0:a=1[b];[b]volume=${volume}`,
  ]

  const toneSprite = generateToneSprite(
    notes.map((n) => getNoteNodeName(n)),
    duration,
    silenceDuration
  )
  const outFileNum = outFiles.length

  // NOTE: Currently creates all outfiles from scratch, i.e. runs the same
  // ffmpeg command for each outfile (only changing the outfile). Would
  // probably be quicker to generate one file, and then convert this file to
  // different formats.
  await Promise.all(
    outFiles.map((outFile, outFileIndex) => {
      const args = [
        "-y", // overwrite files without prompt
        ...silenceArgs,
        ...toneArgs,
        ...filterComplexArgs,
        outFile.toString(),
      ]

      const ffmpegProc = spawn(ffmpegPath, args)
      return new Promise((resolveFunc) => {
        ffmpegProc.stdout.on("data", (data: string) => {
          reporter.verbose(`ffmpeg stderr: ${data}`)
        })
        ffmpegProc.stderr.on("data", (data: string) => {
          reporter.verbose(`ffmpeg stdout: ${data}`)
        })

        ffmpegProc.on("close", (code: number) => {
          if (code === 0) {
            repAct.setStatus(
              `Snapsen -- ffmpeg created tone file ${
                outFileIndex + 1
              }/${outFileNum}: ${outFile}`
            )
          } else {
            repAct.panic(
              `Snapsen -- ffmpeg failed to create tone audio sprites, exited with code ${code}. Run gatsby with '--verbose' to see ffmpeg stdout and stderr.`
            )
            repAct.end()
          }
          resolveFunc(code)
        })
      })
    })
  )
  repAct.setStatus("Snapsen -- ffmpeg created tone audio sprites successfully")
  repAct.end()

  return { audioSpriteMeta: toneSprite }
}

interface CreateToneAudiospritNodesArgs {
  siteNode: Node
  createNode: CreateFileNodeFromBufferArgs["createNode"]
  createNodeId: CreateFileNodeFromBufferArgs["createNodeId"]
  createParentChildLink: gatsby.Actions["createParentChildLink"]
  cache: CreateFileNodeFromBufferArgs["cache"]
  reporter: Reporter
  notes: Note[]
  outFiles: PathLike[]
  toneDuration?: number
  fadeDuration?: number
  silenceDuration?: number
}

export const createToneAudiospriteNodes: any = async function ({
  siteNode,
  createNode,
  createNodeId,
  createParentChildLink,
  cache,
  reporter,
  notes,
  outFiles,
  toneDuration,
  fadeDuration,
  silenceDuration,
}: CreateToneAudiospritNodesArgs) {
  // Set default durations
  const tnDur = toneDuration ? toneDuration : 2
  const fdDur = fadeDuration ? fadeDuration : 0.1
  const slDur = silenceDuration ? silenceDuration : 1

  const { audioSpriteMeta } = await generateTones({
    reporter,
    notes,
    outFiles,
    toneDuration: tnDur,
    fadeDuration: fdDur,
    silenceDuration: slDur,
    harmonics: [1, 3, 5, 7, 9],
  })
  const toneSpriteMeta: FileSystemNode = await createFileNodeFromBuffer({
    parentNodeId: siteNode.id,
    buffer: Buffer.from(JSON.stringify(audioSpriteMeta), "utf-8"),
    name: "snapsenToneAudiospriteMeta",
    ext: ".json",
    createNode,
    createNodeId,
    cache,
  })

  createParentChildLink({
    parent: siteNode,
    child: toneSpriteMeta,
  })

  const outFileNodes = await Promise.all(
    outFiles.map((filePath, fileIndex) => {
      return createAudiospriteFileNode({
        name: "snapsenToneAudiosprite",
        filePath: filePath,
        fileIndex: fileIndex,
        nodeId: toneSpriteMeta.id,
        createNode,
        createNodeId,
        cache,
      })
    })
  )

  outFileNodes.forEach((node) => {
    createParentChildLink({
      parent: toneSpriteMeta,
      child: node,
    })
    reporter.verbose(
      `Snapsen -- Created tone audio sprite file with ID ${node.id}`
    )
  })
}
