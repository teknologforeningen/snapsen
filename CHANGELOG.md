# Snapsen Changelog

Changelog for Snapsen. Version numbering *should be* semantic with regards to:

- Major version: Song metadata and config file.
- Minor version: New features
- Patch version: Bug fixes and improvements

## v4.11.1

### Fixed

- Caching for ThemedSVG components (category icons).
