FROM node:21.6-bullseye-slim

ARG VCS_URL="https://gitlab.com/teknologforeningen/snapsen"
ARG DOC_URL="https://gitlab.com/teknologforeningen/snapsen/-/wikis/home"

LABEL org.opencontainers.image.authors="Teknologföreningen <sangk@tf.fi>"
LABEL org.opencontainers.image.url=$VCS_URL
LABEL org.opencontainers.image.documentation=$DOC_URL
LABEL org.opencontainers.image.vendor="Teknologföreningen"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.title="teknologforeningen/snapsen"
LABEL org.opencontainers.image.description="Snapsen digital song book base image"
LABEL org.opencontainers.image.source=$VCS_URL

ARG SNAPSEN_PORT="8000"
ARG APT_DEPS="git"
ARG CANVAS_DEPS="build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev"
ARG GL_DEPS="python3 libxi-dev libgl1-mesa-dev"
ARG NODE_ENV=production

WORKDIR /snapsen
RUN --mount=type=bind,source=.buildkit-cache/apt,target=/snapsen/.buildkit-cache/apt,rw \
  mkdir -p .buildkit-cache/apt/partial && \
  apt update && \
  apt install -y -o Dir::Cache::Archives=/snapsen/.buildkit-cache/apt \
    ${APT_DEPS} && \
  rm -rf /var/lib/apt/lists/*

COPY yarn.lock package.json .yarnrc.yml ./
COPY .yarn .yarn
ENV npm_config_python=/usr/bin/python3
RUN yarn set version berry \
  && yarn --version \
  && yarn workspaces focus --all --production \
  && yarn install --inline-builds \
  && du -hs /snapsen/.yarn


COPY gatsby-browser.tsx \
  gatsby-config.ts \
  gatsby-node.ts \
  gatsby-ssr.tsx \
  LICENSE \
  README.md \
  tsconfig.json \
  ./

COPY gatsby gatsby
COPY plugins plugins
COPY src src
RUN yarn install

ONBUILD COPY ./ ./

# If this variable is set to "production", the generated robots.txt file
# contains "Allow: /". Otherwise it defaults to "Disallow: /".
ONBUILD ARG GATSBY_ACTIVE_ENV="production"

ONBUILD ARG GATSBY_ARGS="--no-color"
ONBUILD RUN yarn run build ${GATSBY_ARGS}
ONBUILD RUN sed -i -e 's/\$PORT/8000/' public/nginx.conf

ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF

LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.version=$BUILD_VERSION
LABEL org.opencontainers.image.revision=$VCS_REF

CMD ["ls", "-l"]
