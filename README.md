<p align="center">
  <img alt="Snapsen" src="/src/images/snapsen-icon.png" width="60" />
</p>
<h1 align="center">
  Snapsen
</h1>

Snapsen is a digital songbook app based on [Gatsby](https://www.gatsbyjs.com/)
which allows you to view, search and filter songs from various songbooks. In
addition to viewing the lyrics of songs it supports images, audio files, and
sheet music.

### Demo

- **[snapsen-song-book.netlify.app](https://snapsen-song-book.netlify.app/)**

  Demo version deployed to Netlify. Version matches the last release.

- **[sångbok.tf.fi](https://xn--sngbok-iua.tf.fi/)**

  Example of a production instance.
  [Teknologföreningen's](https://teknologforeningen.fi/) digital song book.

### Notable features

- Generate a static web app from Markdown files (using [Gatsby](https://www.gatsbyjs.com/))
  - Custom [Markdown syntax](plugins/gatsby-remark-lyrics) for song lyrics
    (using [remark-directive](https://github.com/remarkjs/remark-directive))
- Customisable color scheme with dark mode
  - Monochrome SVG images (consisting of only `#000000` and transparency)
    automatically match the color scheme both in light and dark mode (see
    [here](https://snapsen-song-book.netlify.app/local-test-book-imbelupet/) for
    a live example)
- Display sheet music
  - SVG images generated from MXL files using
    [OpenSheetMusicDisplay](https://opensheetmusicdisplay.org/))
  - Sheet music also matches color scheme both in light and dark mode
- Audio player with per-voice volume and panning controls, ideal for practising
  choral songs
  - Made possible with [Howler.js](https://howlerjs.com/)
  - For a live example see
    [here](https://snapsen-song-book.netlify.app/local-test-book-norges-beste/)
- Local full-text search and filtering with tags
  - Search is handled by [Flexsearch](https://github.com/nextapps-de/flexsearch)
  - Search field is based on [react-select](https://react-select.com/home))

## Deploy to Netlify

If you want to deploy your own Snapsen instance to Netlify, press this nifty
button:

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/teknologforeningen/snapsen)

In order to use the Netlify CMS with Snapsen, you need to configure the [Git
Gateway](https://docs.netlify.com/visitor-access/git-gateway/) integration. This
is configured in the Netlify admin interface at

> **Site settings > Identity > Services > Git Gateway**

NOTE: The automatically generated GitLab personal access token expires after a
short time. For a more permanent solution, create a [project access
token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)
for your repository.

### Netlify configuration

The netlify configuration is located in
[static/admin/config.yml](static/admin/config.yml). Edit this file according to
your needs.

A `robots.txt` file will be automatically generated using
[gatsby-plugin-robots.txt](https://www.gatsbyjs.com/plugins/gatsby-plugin-robots-txt/).
In production you should set the environment variable `GATSBY_ACTIVE_ENV:
production` in order to make the file contain the line `Allow: /`. This
instructs web crawlers to index your site.

## Quick start using Docker

If you prefer to use Docker for building/running your own Snapsen instance, see
[this
repository](https://gitlab.com/teknologforeningen/snapsen-example-songbook) for
instructions.

## Quick start, local development

1. **Install [Node.js](https://nodejs.org/en/), [git](https://git-scm.com/), and
   [yarn](https://yarnpkg.com/en/)**

   Through your package manager or e.g. Homebrew if running macOS.

1. **Clone the repository and create your own config file**

   First clone the repository. Then copy the default configuration to
   `./config.yml` and edit it according to your own preferences. Any option not
   set in `config.yml` will be set by the default `gatsby/defaultConfig.yml`
   config.

   For a list of possible configuration options and their documentation, see the
   comments in `gatsby/defaultConfig.yml`.

   ```sh
   git clone git@gitlab.com:teknologforeningen/snapsen.git
   cd snapsen
   cp gatsby/defaultConfig.yml config.yml
   ```

1. **Start developing.**

   The environment variable `NODE_OPTIONS=--no-experimental-fetch` needs to be
   exported due to [this bug](https://github.com/parcel-bundler/parcel/issues/8005).

   ```sh
   export NODE_OPTIONS=--no-experimental-fetch
   yarn
   yarn start
   ```

1. **Open the source code and start editing!**

   Snapsen is now running at `http://localhost:8000`!

   Open the `snapsen` directory in your code editor of choice and edit
   `src/pages/index.tsx`. Save your changes and the browser will update in real
   time!
