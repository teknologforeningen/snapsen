// ----- gatsby-config.js -----------------------------------------------------

import { resolve } from "path"
import { siteMetadata, bookPlugins } from "./gatsby/config"
import type { GatsbyConfig } from "gatsby"
interface PageContext {
  songSlug: string
  index: number
  bookSlug: string
  language: string
  i18n: {
    language: string
    languages: string[]
    defaultLanguage: string
    generateDefaultLanguagePage: boolean
    routed: boolean
    originalPath: string
    path: string
  }
}

interface SitePage {
  path: string
  pageContext: PageContext
}
interface SerializeProps {
  path?: string
  pageContext: PageContext
}

const config: GatsbyConfig = {
  flags: {
    DEV_SSR: true,
  },
  siteMetadata,
  trailingSlash: `always`,
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-dark-mode`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/locales`,
        name: `locale`,
      },
    },
    {
      resolve: `gatsby-plugin-react-i18next`,
      options: {
        localeJsonSourceName: `locale`, // name given to `gatsby-source-filesystem` plugin.
        languages: siteMetadata.languages,
        defaultLanguage: siteMetadata.defaultLanguage,
        redirect: siteMetadata.redirectLanguage,
        siteUrl: siteMetadata.siteUrl,
        // if you are using trailingSlash gatsby config include it here, as well (the default is 'always')
        trailingSlash: "always",
        // you can pass any i18next options
        i18nextOptions: {
          interpolation: {
            escapeValue: false, // not needed for react as it escapes by default
          },
          keySeparator: false,
          nsSeparator: false,
        },
        pages: [
          {
            matchPath: "/dev-404-page/",
            languages: ["en"],
          },
        ],
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-lyrics`,
          `gatsby-remark-copy-linked-files`,
          {
            resolve: `gatsby-remark-smartypants`,
            options: {
              dashes: `oldschool`,
              quotes: true,
              ellipses: true,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: resolve(`src/images`),
      },
    },
    {
      resolve: `gatsby-transformer-yaml`,
      options: {
        typeName: `YamlBook`,
      },
    },
    {
      resolve: `gatsby-transformer-json`,
      options: {
        typeName: `AudiospriteJson`,
        path: `./.cache/`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      // TODO: Use localization and generated icon
      // https://www.gatsbyjs.com/plugins/gatsby-plugin-manifest/#localization-configuration
      options: {
        name: `${siteMetadata.title}`,
        short_name: `${siteMetadata.title}`,
        start_url: `/`,
        background_color: `${siteMetadata.colorsDark.bg}`,
        theme_color: `${siteMetadata.colorsDark.fg}`,
        display: `fullscreen`,
        icon: `${siteMetadata.appIconPath}`,
      },
    },
    `gatsby-plugin-remove-serviceworker`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: `@vtex/gatsby-plugin-nginx`,
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        resolveEnv: () => process.env.GATSBY_ACTIVE_ENV,
        env: {
          development: {
            policy: [{ userAgent: "*", disallow: ["/"] }],
          },
          production: {
            policy: [{ userAgent: "*", allow: "/", disallow: "/admin/" }],
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        excludes: ["/**/404", "/**/404.html", "/404.html", "/404"],
        query: `
        {
          allSitePage {
            nodes {
              path
              pageContext
            }
          }
        }
        `,
        resolveSiteUrl: () => siteMetadata.siteUrl,
        resolvePages: ({ allSitePage: { nodes: allPages } }) => {
          return allPages
            .filter((page: SitePage) => {
              // Filter i18n routed pages so that they are not added to sitemap.
              if (page.pageContext.i18n.routed) {
                return false
              } else {
                return true
              }
            })
            .map((page: SitePage) => {
              return { ...page }
            })
        },
        serialize: ({ pageContext }: SerializeProps) => {
          console.log(
            "Adding page to sitemap: " + pageContext.i18n.originalPath
          )

          const { languages, originalPath, defaultLanguage } = pageContext.i18n
          const url = new URL(originalPath, siteMetadata.siteUrl)
          const links = [
            { lang: defaultLanguage, url },
            { lang: "x-default", url },
          ]

          // Add links to the same page in other languages
          languages.forEach((lang: string) => {
            if (lang === defaultLanguage) return
            const langUrl = new URL(
              `${lang}${originalPath}`,
              siteMetadata.siteUrl
            )
            links.push({ lang, url: langUrl })
          })

          // Add index page to sitemap with higher priority
          return {
            url: url,
            changefreq: originalPath === "/" ? "daily" : "weekly",
            priority: originalPath === "/" ? 1.0 : 0.7,
            links,
          }
        },
      },
    },
    `gatsby-plugin-netlify`,
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
        enableIdentityWidget: true,
      },
    },
  ],
}

config?.plugins?.push(...bookPlugins)

//module.exports = {
//  pathPrefix: `/snapsen`,
//}

export default config
